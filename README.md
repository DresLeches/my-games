# My Games

These are all the games I have worked from the video game programming class. This was the
class that got me more interested in game development. Looking at the codebase, I really
do like how much I've grown and improved. Moreover, looking at how my game engine side project 
I'm doing right now makes me feel proud and happy that I'm doing something that I enjoy and 
grow with.
