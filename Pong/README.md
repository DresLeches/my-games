# My First Game

This was my first time making a video game. Like many others, I created a Pong Game to get the
feel of what it's like making a game. At that time, while I loved playing video games, I never
really developed an interest in making video games. I wanted to give it a try and see how it goes.
In the end, I had a functional game, but looking at the code again, I realized I honestly had no
idea what I was doing and my C++ skill needed more refining. On the other hand, it's remarkable
to see how far I have come and what I'm working on now.

Here's what's mostly been implemented
* Created a simple window using SDL
* Did the math to work out the ball physics
