#include "Game.h"

int main(int argc, char** argv)
{
    Game *g = new Game();
    if (g->initialize()) {
        g->runLoop();
        g->shutDown();
    }
    delete g;
	return 0;
}
