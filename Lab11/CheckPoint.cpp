//
//  CheckPoint.cpp
//  Game-mac
//
//  Created by Pablo Chung on 4/12/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "CheckPoint.hpp"
#include "MeshComponent.h"
#include "Mesh.h"
#include "Game.h"
#include "CollisionComponent.h"
#include "Player.h"
#include "Renderer.h"
#include <iostream>

CheckPoint::CheckPoint(class Game* game)
:Actor(game)
,isCurrentCP(false)
{
    mMesh = new MeshComponent(this);
    mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Checkpoint.gpmesh"));
    
    mCollision = new CollisionComponent(this);
    mCollision->SetSize(25.0f, 25.0f, 25.0f);
    
    
}

CheckPoint::~CheckPoint() {
    
}

// Check if there was a collision. If so, remove the thing off the queue
void CheckPoint::UpdateActor(float deltaTime) {
    if (mCollision->Intersect(mGame->GetPlayer()->GetCollision())) {
        std::cout << "Touch" << std::endl;
        
        if (!mLevelString.empty()) {
            mGame->SetNextLevel(mLevelString);
        }
        
        // Sound for the checkpoint
        Mix_Chunk* mc = mGame->GetSound("Assets/Sounds/Checkpoint.wav");
        Mix_PlayChannel(-1, mc, 0);
        
        // Get a reference to the queue
        std::queue<CheckPoint*>& tempCPQueue = mGame->GetCheckpointQueue();
        
        // Get rid of the old checkpoint and update the respawn location
        CheckPoint* cp = tempCPQueue.front();
        cp->SetState(Actor::EDead);
        cp->setCurrentCP(false);
        Vector3 cpPos = cp->GetPosition();
        mGame->GetPlayer()->SetRespawnPos(cpPos);
        tempCPQueue.pop();
        
        // Get the new Checkpoint if there is enough checkpoint
        if (!tempCPQueue.empty()) {
            CheckPoint* newCP = tempCPQueue.front();
            newCP->setCurrentCP(true);
            newCP->GetMesh()->SetTextureIndex(0);
            mGame->SetCurrCP(newCP);
        }
    }
}
