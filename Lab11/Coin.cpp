//
//  Coin.cpp
//  Game-mac
//
//  Created by Pablo Chung on 4/13/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Coin.hpp"
#include "MeshComponent.h"
#include "Game.h"
#include "Renderer.h"
#include "Player.h"
#include "CollisionComponent.h"
#include "MoveComponent.h"

Coin::Coin(class Game* game)
:Actor(game)
{
    mMesh = new MeshComponent(this);
    mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Coin.gpmesh"));
    
    mCollision = new CollisionComponent(this);
    mCollision->SetSize(100.0f, 100.0f, 100.0f);
    
    mMove = new MoveComponent(this);
    mMove->SetAngularSpeed(Math::Pi);
}

Coin::~Coin() {
    
}

void Coin::UpdateActor(float deltaTime) {
    SetRotation(GetRotation() + mMove->GetAngularSpeed() * deltaTime);
    
    if (GetCollision()->Intersect(mGame->GetPlayer()->GetCollision())) {
        // Sound for the coin
        Mix_Chunk* mc = mGame->GetSound("Assets/Sounds/Coin.wav");
        Mix_PlayChannel(-1, mc, 0);
        
        SetState(Actor::EDead);
    }
}
