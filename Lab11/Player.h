#pragma once
#include "Actor.h"

class Player : public Actor
{
public:
	Player(class Game* game);
    void SetRespawnPos(Vector3 respawn) { mRespawn = respawn; }
    Vector3 GetRespawnPos() { return mRespawn; }
private:
    Vector3 mRespawn;
};
