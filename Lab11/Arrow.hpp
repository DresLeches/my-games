//
//  Arrow.hpp
//  Game-mac
//
//  Created by Pablo Chung on 4/13/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Arrow_hpp
#define Arrow_hpp

#include <stdio.h>
#include "Actor.h"

class Arrow : public Actor {
public:
    Arrow(class Game* g);
    ~Arrow();
    void UpdateActor(float deltaTime);
};

#endif /* Arrow_hpp */
