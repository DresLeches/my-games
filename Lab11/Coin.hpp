//
//  Coin.hpp
//  Game-mac
//
//  Created by Pablo Chung on 4/13/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Coin_hpp
#define Coin_hpp

#include <stdio.h>
#include "Actor.h"

class Coin : public Actor {
public:
    Coin(class Game* game);
    ~Coin();
    void UpdateActor(float deltaTime);
};
#endif /* Coin_hpp */
