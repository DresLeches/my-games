#pragma once
#include "SDL/SDL.h"
#include "SDL/SDL_mixer.h"
#include <unordered_map>
#include <string>
#include <vector>
#include <queue>
#include "Math.h"

class Game
{
public:
	Game();
	bool Initialize();
	void RunLoop();
	void Shutdown();

	void AddActor(class Actor* actor);
	void RemoveActor(class Actor* actor);

	void LoadSound(const std::string& fileName);
	Mix_Chunk* GetSound(const std::string& fileName);

	void LoadLevel(const std::string& fileName);

	class Renderer* GetRenderer() {	return mRenderer; }
    
    std::queue<class CheckPoint*>& GetCheckpointQueue() { return cp_q; }
    
    void SetPlayer(class Player* player) { p = player; }
    class Player* GetPlayer() { return p; }
    
    void SetArrow(class Arrow* arrow) { mArrow = arrow; }
    class Arrow* GetArrow() const { return mArrow; }
    
    void SetCurrCP(class CheckPoint* cp) { currCP = cp; }
    class CheckPoint* GetCurrCP() { return currCP; }
    
    void SetNextLevel(std::string nextLevel) { mNextLevel = nextLevel; }
    
    void LoadNextLevel();

	std::vector<class Block*> mBlocks;
	class Tank* mPlayerOne;
	class Tank* mPlayerTwo;
private:
	void ProcessInput();
	void UpdateGame();
	void GenerateOutput();
	void LoadData();
	void UnloadData();

	// Map of textures loaded
	std::unordered_map<std::string, SDL_Texture*> mTextures;
	std::unordered_map<std::string, Mix_Chunk*> mSounds;
    
    // Player point
    class Player* p;
    
    // Arrow point
    class Arrow* mArrow;
    
    // Current checkpoint
    class CheckPoint* currCP;

	// All the actors in the game
	std::vector<class Actor*> mActors;

	class Renderer* mRenderer;

	Uint32 mTicksCount;
	bool mIsRunning;
    
    // Queue of checkpoint
    std::queue<class CheckPoint*> cp_q;
    
    // Next level string
    std::string mNextLevel;
    
    // Mix Chunk
    class Mix_Chunk* mc;
};
