//
//  CheckPoint.hpp
//  Game-mac
//
//  Created by Pablo Chung on 4/12/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef CheckPoint_hpp
#define CheckPoint_hpp

#include <stdio.h>
#include <string>
#include "Actor.h"

class CheckPoint : public Actor {
public:
    CheckPoint(class Game* game);
    ~CheckPoint();
    
    // Set/Getter Checkpoint
    void setCurrentCP(bool cpState) { isCurrentCP = cpState; }
    bool getCurrentCP() { return isCurrentCP; }
    
    // Set Level String
    void SetLevelString(std::string levelString) { mLevelString = levelString; }
    
    void UpdateActor(float deltaTime);
private:
    bool isCurrentCP;
    std::string mLevelString;
};

#endif /* CheckPoint_hpp */
