//
//  Arrow.cpp
//  Game-mac
//
//  Created by Pablo Chung on 4/13/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Arrow.hpp"
#include "MeshComponent.h"
#include "Mesh.h"
#include "Game.h"
#include "Renderer.h"
#include "Player.h"
#include "CheckPoint.hpp"

/*
 mMesh = new MeshComponent(this);
 mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Cube.gpmesh"));
 mScale = 64.0f;
 
 mCollision = new CollisionComponent(this);
 mCollision->SetSize(1.0f, 1.0f, 1.0f);
 
 game->mBlocks.emplace_back(this);
 */
Arrow::Arrow(class Game* game)
:Actor(game)
{
    mMesh = new MeshComponent(this);
    mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Arrow.gpmesh"));
    
    mScale = 0.15f;
    
}

Arrow::~Arrow() {
    
}

void Arrow::UpdateActor(float deltaTime) {
    
    if (!mGame->GetCheckpointQueue().empty()) {
        // Vectors that we need to compute the vector
        Vector3 playerPos = mGame->GetPlayer()->GetPosition();
        Vector3 cpPos = mGame->GetCurrCP()->GetPosition();
        Vector3 playerToCP = cpPos - playerPos;
    
        playerToCP = Vector3::Normalize(playerToCP);
        Vector3 xAxis(1.0, 0.0f, 0.0f);
    
        // Angle of rotation
        float dotProd = Vector3::Dot(playerToCP, xAxis);
        float angleRot = Math::Acos(dotProd);
    
        // Axis of rotation
        Vector3 axisRot = Vector3::Cross(xAxis, playerToCP);
        axisRot = Vector3::Normalize(axisRot);
    
        // Boolean cross product
        bool isLengthNearZero = Math::NearZero(axisRot.Length());
    
        // Construct the quaternion
        mQuat = (mGame->GetCurrCP() != nullptr || isLengthNearZero) ? Quaternion(axisRot, angleRot) : Quaternion::Identity;
    
        
    }
    else {
        mQuat = Quaternion::Identity;
    }
    // Set the position of the arrow
    Vector3 sp(0.0f, 250.0f, 0.1f);
    Vector3 arrowPos = mGame->GetRenderer()->Unproject(sp);
    SetPosition(arrowPos);
}
