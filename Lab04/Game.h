#pragma once

// Header files
#include "SDL/SDL.h"
#include "Actor.h"
//#include "Block.h"
#include <cmath>
#include <vector>
#include <unordered_map>
#include <string>
#include <fstream>

class Game {
public:
    Game();
    bool initialize();
    void shutDown();
    void runLoop();
    
    // ProcessInput, UpdateGame, GenerateOuput)
    void processInput();
    void updateGame();
    void generateOutput();
    
    // Manipulating the actors
    void AddActor(Actor* actor);
    void RemoveActor(Actor* actor);
    
    // Manipulating the sprites
    void AddSprite(SpriteComponent* sc);
    void RemoveSprite(SpriteComponent* sc);
    void AddBlock(class Block* b);
    void RemoveBlock(class Block* b);
    
    // Creating the texture
    //LoadTexture takes in the name of an image file, runs the image loading code, and adds the texture to your map
    //GetTexture takes in the name of an image file, and returns the texture from the map, if it exists.
    void LoadTexture(std::string imageName);
    SDL_Texture* GetTexture(std::string imageName);
    
    // Get the lbokc
    std::vector<Block*> GetBlock() { return blocks; }
    
    // Parse the text
    void ParseFile(std::string filename);
    
    // Get the paddle
    class Paddle* GetPaddle() { return paddle; }
    
    
private:
    // Window dim
    int _w;
    int _h;
    
    // Window object
    SDL_Window* sdlw;
    SDL_Renderer* sdlr;
    
    // SDL Texture
    SDL_Texture *bgt;
    std::unordered_map<std::string, SDL_Texture*> sprites;
     
    // Player quit
    bool playerQuit;
    bool playerLoses;
    
    // Calculating the frame rate
    float delta;
    float prevFrameTime;
    int imgInit;
    Uint32 mTicksCount;
    
    // Actors
    std::vector<Actor*> actors;
    
    // Game Sprite
    std::vector<SpriteComponent*> mSprites;
    
    // Loading the data
    void LoadData();
    void UnloadData();
    
    // Block data
    char fileData[15][15];
    
    // Ball and paddle
    class Ball* ball;
    class Paddle* paddle;
    std::vector<class Block*> blocks;
    
};

