//
//  Ball.h
//  Game-mac
//
//  Created by Pablo Chung on 2/6/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Ball_h
#define Ball_h

#include "Actor.h"

class Ball : public Actor {
public:
    Ball(class Game* game);
    ~Ball() { }
    void UpdateActor(float deltaTime);
private:
    /*
    SpriteComponent* mSprite;
    class BallMove* mMove;
    CollisionComponent* mColl;
     */
    class BallMove* mMove;
};


#endif /* Ball_h */
