//
//  PaddleMove.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/6/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "PaddleMove.h"
#include "Paddle.h"
#include <iostream>

PaddleMove::PaddleMove(class Paddle* owner)
    : MoveComponent(owner), mOwner(owner)
{ }

void PaddleMove::Update(float deltaTime) {
    MoveComponent::Update(deltaTime);
    
    // Cancel it out the change in position if it is going out of bound
    Vector2 vec = mOwner->GetPosition();
    int paddleEnd = mOwner->GetSprite()->GetTexWidth() / 2;
    if (vec.x - paddleEnd < 32 ) {
        Vector2 unitVec = mOwner->GetFoward();
        unitVec = unitVec * (-MoveComponent::GetForwardSpeed() * deltaTime);
        Vector2 newPos = mOwner->GetPosition() + unitVec;
        mOwner->SetPosition(newPos);
    }
    else if (vec.x + paddleEnd > 992) {
        Vector2 unitVec = mOwner->GetFoward();
        unitVec = unitVec * (-MoveComponent::GetForwardSpeed() * deltaTime);
        Vector2 newPos = mOwner->GetPosition() + unitVec;
        mOwner->SetPosition(newPos);
    }
}
