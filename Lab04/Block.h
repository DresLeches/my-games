//
//  Block.h
//  Game-mac
//
//  Created by Pablo Chung on 2/1/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//
#ifndef Block_h
#define Block_h

#include "Actor.h"

class Block : public Actor {// : public Actor {
public:
    Block(class Game* game);
    ~Block();
    void SetTexture(SpriteComponent* sprite);
private:
    Game* mGame;
    //SpriteComponent* mSprite;
    //CollisionComponent* mColl;
};

#endif /* Block_h */
