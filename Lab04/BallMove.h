//
//  BallMove.h
//  Game-mac
//
//  Created by Pablo Chung on 2/6/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef BallMove_h
#define BallMove_h

#include "MoveComponent.h"
#include "Math.h"

class BallMove : public MoveComponent {
public:
    BallMove(class Ball* owner);
    void Update(float deltaTime);
    void SetVelocity(Vector2 newVel);
    void HitsBlock();
private:
    class Ball* mOwner;
    Vector2 vel;
};

#endif /* BallMove_h */
