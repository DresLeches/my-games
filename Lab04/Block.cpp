//
//  Block.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/1/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Block.h"

Block::Block(class Game* game)
    : Actor(game)
{
    mGame = game;
    mSprite = new SpriteComponent(this);
    mColl = new CollisionComponent(this);
    mColl->SetSize(64, 32);
    mGame->AddBlock(this);
    SetSprite(mSprite);
    SetCollComponent(mColl);
}

Block::~Block() {
    mGame->RemoveBlock(this);
}

void Block::SetTexture(SpriteComponent *sprite) {
    mSprite = sprite;
}
