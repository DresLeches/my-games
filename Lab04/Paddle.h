//
//  Paddle.h
//  Game-mac
//
//  Created by Pablo Chung on 2/5/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Paddle_h
#define Paddle_h

#include "Actor.h"

class Paddle : public Actor {
public:
    Paddle(class Game* game);
    ~Paddle();
    void ActorInput(const Uint8* keyState);
    bool IsTouchingWall();
private:
    class PaddleMove* mMove;
    /*
    SpriteComponent* mSprite;
    class PaddleMove* mMove;
    CollisionComponent* mColl;
     */
};


#endif /* Paddle_h */
