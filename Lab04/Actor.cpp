#include "Actor.h"
#include "Component.h"
#include <algorithm>
#include <iostream>

// Create the constructors
Actor::Actor(Game* game)
	:mGame(game)
	,mState(EActive)
	,mPosition(Vector2::Zero)
	,mScale(1.0f)
	,mRotation(0.0f)
    ,mSprite(nullptr)
    ,mMove(nullptr)
    ,mColl(nullptr)
{
	// TODO
    mGame->AddActor(this);
}

// Kill off the actors but not really
Actor::~Actor()
{
	// TODO
    mGame->RemoveActor(this);
    delete mSprite;
    delete mMove;
    delete mColl;
    
    //mState = EDead;
}

// Update the actor
void Actor::Update(float deltaTime)
{
    // TODO
    if (mState == EActive) {
        
        // Update the move component
        if (mMove) {
            mMove->Update(deltaTime);
        }
        // Update the spirte component
        if (mSprite) {
            mSprite->Update(deltaTime);
        }
        if (mColl) {
            mColl->Update(deltaTime);
        }
        UpdateActor(deltaTime);
    }
}

// Update the actor
void Actor::UpdateActor(float deltaTime)
{
}

// Process the actor input
void Actor::ProcessInput(const Uint8* keyState)
{
	// TODO
    if (mState == EActive) {
        if (mMove) {
            mMove->ProcessInput(keyState);
        }
        if (mSprite) {
            mSprite->ProcessInput(keyState);
        }
        if (mColl) {
            mColl->ProcessInput(keyState);
        }
        ActorInput(keyState);
    }
}

// Actor input
void Actor::ActorInput(const Uint8* keyState)
{
}

// Actor get forward
Vector2 Actor::GetFoward() {
    Vector2 unitVec(Math::Cos(GetRotation()), -Math::Sin(GetRotation()));
    return unitVec;
}



