//
//  BallMove.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/6/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "BallMove.h"
#include "Ball.h"
#include "Block.h"
#include <iostream>
#include "Paddle.h"


BallMove::BallMove(class Ball* owner)
    : MoveComponent(owner), mOwner(owner), vel(250, -250)
{ }

void BallMove::Update(float deltaTime) {
    // Set the movement of the ball
    
    // First check if a paddle collision has occurred
    Paddle* p = mOwner->GetGame()->GetPaddle();
    if (mOwner->GetCollComp()->Intersect(p->GetCollComp())) {
        if (mOwner->GetCollComp()->IsLeftOneThird(p->GetCollComp())) {
            Vector2 n(Math::Cos(120.0f), Math::Sin(170.0f));
            vel = Vector2::Reflect(vel, n);
        }
        else if (mOwner->GetCollComp()->IsRightOneThird(p->GetCollComp())) {
            Vector2 n(Math::Cos(40.0f), Math::Sin(10.0f));
            vel = Vector2::Reflect(vel, n);
        }
        else {
            vel.y = -vel.y;
        }
    }
     
    
    // Now check if a block collision has occurred
    std::vector<Block*> blocks = mOwner->GetGame()->GetBlock();
    for (Block* b : blocks) {
        if (mOwner->GetCollComp()->Intersect(b->GetCollComp())) {
            b->SetState(Actor::EDead);
        }
    }
    
    Vector2 changeInPosition = vel * deltaTime;
    Vector2 newPos = mOwner->GetPosition() + changeInPosition;
    
    // Change the position and the velocity
    if (newPos.x - 11 <= 32) {
        vel.x = -vel.x;
        newPos.x = 32 + 11;
    }
    else if (newPos.x + 11 >= 992) {
        vel.x = -vel.x;
        newPos.x = 992 - 11;
    }
    if (newPos.y - 11 <= 32) {
        vel.y = -vel.y;
        newPos.y = 32 + 11;
    }
    // For debugging
    else if (newPos.y + 11 >= 768) {
        vel.x = 250;
        vel.y = -250;
    }
    
    mOwner->SetPosition(newPos);
}

void BallMove::SetVelocity(Vector2 newVel) {
    vel = newVel;
}
