#pragma once
#include "Component.h"
#include "Math.h"

class CollisionComponent : public Component
{
public:
	CollisionComponent(class Actor* owner);
	~CollisionComponent();

	// Set width/height of this box
	void SetSize(float width, float height)
	{
		mWidth = width;
		mHeight = height;
	}
    
    // Return the width and height
    float GetWidth() { return mWidth; }
    float GetHeight() { return mHeight; }

	// Returns true if this box intersects with other
	bool Intersect(const CollisionComponent* other);

	// Get min and max points of box
	Vector2 GetMin() const;
	Vector2 GetMax() const;

	// Get width, height, center of box
	const Vector2& GetCenter() const;
	float GetWidth() const { return mWidth; }
	float GetHeight() const { return mHeight; }
    
    // Check to see if the collision was 1/3 of the edges
    bool IsLeftOneThird(const CollisionComponent* other);
    bool IsRightOneThird(const CollisionComponent* other);
    
private:
	float mWidth;
	float mHeight;
};

