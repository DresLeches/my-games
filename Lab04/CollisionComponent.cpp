#include "CollisionComponent.h"
#include "Actor.h"
#include <iostream>


CollisionComponent::CollisionComponent(class Actor* owner)
:Component(owner)
,mWidth(0.0f)
,mHeight(0.0f)
{
	
}

CollisionComponent::~CollisionComponent()
{
	
}

bool CollisionComponent::Intersect(const CollisionComponent* other)
{
    
	// TODO: Implement
    
    Vector2 blockA_min = GetMin();
    Vector2 blockB_min = other->GetMin();
    Vector2 blockA_max = GetMax();
    Vector2 blockB_max = other->GetMax();

    if (blockA_max.x < blockB_min.x) {
        return false;
    }
    else if (blockB_max.x < blockA_min.x) {
        return false;
    }
    else if (blockA_max.y < blockB_min.y) {
        return false;
    }
    else if (blockB_max.y < blockA_min.y) {
        return false;
    }
    return true;
}

Vector2 CollisionComponent::GetMin() const
{
	// TODO: Implement
    Vector2 min;
    Vector2 ownerPos = mOwner->GetPosition();
    min.x = ownerPos.x - (mWidth * mOwner->GetScale()) / 2.0f;
    min.y = ownerPos.y - (mHeight * mOwner->GetScale()) / 2.0f;
	return min;
}

Vector2 CollisionComponent::GetMax() const
{
	// TODO: Implement
    Vector2 max;
    Vector2 ownerPos = mOwner->GetPosition();
    max.x = ownerPos.x + (mWidth * mOwner->GetScale()) / 2.0f;
    max.y = ownerPos.y + (mHeight * mOwner->GetScale()) / 2.0f;
	return max;
}

const Vector2& CollisionComponent::GetCenter() const
{
	return mOwner->GetPosition();
}

bool CollisionComponent::IsLeftOneThird(const CollisionComponent* other) {
    Vector2 paddleCenter = GetCenter();
    Vector2 ballCenter = other->GetCenter();
    
    float paddleEdgeX = paddleCenter.x - (other->mWidth * mOwner->GetScale()) / 2.0f;
    float paddleOneThirdX = paddleCenter.x - (1.0f * (other->mWidth * mOwner->GetScale())) / 6.0f;
    
    return (paddleEdgeX <= ballCenter.x && paddleOneThirdX >= ballCenter.x);
}

bool CollisionComponent::IsRightOneThird(const CollisionComponent* other) {
    Vector2 paddleCenter = GetCenter();
    Vector2 ballCenter = other->GetCenter();
    
    float paddleEdgeX = paddleCenter.x + (other->mWidth * mOwner->GetScale()) / 2.0f;
    float paddleOneThirdX = paddleCenter.x + (1.0f * (other->mWidth * mOwner->GetScale())) / 6.0f;
    
    return (paddleEdgeX >= ballCenter.x && paddleOneThirdX <= ballCenter.x);
}



