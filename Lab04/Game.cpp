//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
#include "Block.h"
#include "Paddle.h"
#include "Ball.h"
#include <stdio.h>
#include <iostream>
#include <iostream>
#include <SDL/SDL_image.h>
#include <algorithm>

std::vector<std::vector<Block*> > blocks;

// TODO
Game::Game() { }

// Initialize the game
bool Game::initialize() {
    
    // It didn't work :(
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return false;
    }
    
    // Initialize the game's member variables
    playerQuit = false;
    playerLoses = false;

    // Initialize the windows dimensions
    _w = 1024;
    _h = 768;
    
    // Creating the SDL Window
    sdlw = SDL_CreateWindow("Asteroids (Not Really)",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED,
                            _w,
                            _h,
                            0);
    
    // Create the SDL Renderer
    sdlr = SDL_CreateRenderer(sdlw, 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    
    // Load the image
    int flags = IMG_INIT_PNG;
    int imgInit = IMG_Init(flags);
    if((imgInit & flags) != flags) {
        printf("IMG_Init: Failed to init required png support!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
        return false;
    }
    
    // Loading the actor data
    LoadData();
    
    // Get the number of ticks
    mTicksCount = SDL_GetTicks();
    
    // We created it :)
    return true;
}

// Shutdown the game
void Game::shutDown() {
    UnloadData();
    SDL_DestroyRenderer(sdlr);
    SDL_DestroyWindow(sdlw);
    SDL_Quit();
    IMG_Quit();
}

// Run the loop
void Game::runLoop() {
    while (!playerQuit && !playerLoses) {
        processInput();
        updateGame();
        generateOutput();
    }
}

// Process the input
void Game::processInput() {
    SDL_Event sdlEvent;
    bool quit = false;
    
    while (SDL_PollEvent(&sdlEvent)) {
        switch(sdlEvent.type) {
            case SDL_QUIT:
                quit = true;
                break;
            default:
                // Do Nothing
                break;
        }
    }
    // Get the keyboard state
    const Uint8 *state = SDL_GetKeyboardState(NULL);
    
    // Iterating through all the actors and proess their input
    
    unsigned long len = actors.size();
    for (int i = 0; i < len; i++) {
        (actors[i])->ProcessInput(state);
    }
    
    if (state[SDL_SCANCODE_ESCAPE] || quit) {
        playerQuit = true;
    }
}

// Update the game
void Game::updateGame() {
    // Compute delta time
    Uint32  tickNow = SDL_GetTicks();
    // Wait until 16ms has elapsed since last frame
    while (tickNow - mTicksCount < 16) {
        tickNow = SDL_GetTicks();
    }
    
    // Get deltaTime in seconds
    delta = (tickNow - mTicksCount) / 1000.0f;
    
    // Don't let deltaTime be greater than 0.05f (50 ms)
    if (delta > 0.05f) {
        delta = 0.05f;
    }
    mTicksCount = tickNow;
    
    // Copy the actors and update them
    std::vector<Actor*> copy = actors;
    for (auto actor : copy) {
        actor->Update(delta);
    }
    
    // Kill of the dead dudes
    std::vector<Actor*> tempDeadActors;
    for (auto actor : actors) {
        if (actor->GetState() == Actor::EDead) {
            tempDeadActors.emplace_back(actor);
        }
    }
    for (auto actor : tempDeadActors) {
        delete actor;
    }
    
}

// Generate the output
void Game::generateOutput() {
    if (SDL_SetRenderDrawColor(sdlr, 0, 0, 255, 255) != 0) {
        SDL_Log("Unable to generate output SDL: %s", SDL_GetError());
        return;
    }
    if (SDL_RenderClear(sdlr)) {
        SDL_Log("Unable to clear renderer SDL: %s", SDL_GetError());
        return;
    }
    
    for (auto sprite : mSprites) {
        sprite->Draw(sdlr);
    }
    // Render the image
    SDL_RenderPresent(sdlr);
}

// Add actor
void Game::AddActor(Actor* actor) {
    actors.emplace_back(actor);
}

// Remove actor
/*
 auto iter = std::find(actors.begin(), actors.end(), actor);
 if (iter != actros.end())
 {
 // Swap to end of vector and pop off (avoid erase copies)
 auto iter2 = actors.end() - 1;
 std::iter_swap(iter, iter2);
 actors.pop_back();
 }
 */
void Game::RemoveActor(Actor* actor) {
    auto iter = std::find(actors.begin(), actors.end(), actor);
    if (iter != actors.end()) {
        // Swap to end of vector and pop off (avoid erase copies)
        auto iter2 = actors.end() - 1;
        std::iter_swap(iter, iter2);
        actors.pop_back();
    }
}

/*
 // Create test actors
 Actor* test = new Actor(this);
 test->SetPosition(Vector2(512.0f, 384.0f));
 SpriteComponent* sc = new SpriteComponent(test, 1);
 sc->SetTexture(GetTexture("Assets/Stars.png"));
 
 mShip = new Ship(this);
 mShip->SetPosition(Vector2(512.0f, 384.0f));
 */
// Load the data
void Game::LoadData() {
    
    // Load all the textures
    LoadTexture("Assets/Background.png");
    LoadTexture("Assets/Ball.png");
    LoadTexture("Assets/BlockA.png");
    LoadTexture("Assets/BlockB.png");
    LoadTexture("Assets/BlockC.png");
    LoadTexture("Assets/BlockD.png");
    LoadTexture("Assets/BlockE.png");
    LoadTexture("Assets/BlockF.png");
    LoadTexture("Assets/Paddle.png");
    
    Actor* bg = new Actor(this);
    bg->SetPosition(Vector2(_w/2, _h/2));
    SpriteComponent* sc = new SpriteComponent(bg);
    sc->SetTexture(GetTexture("Assets/Background.png"));
    
    // Create the paddle
    paddle = new Paddle(this);
    //paddle->SetPosition(Vector2(_w/2, _h - 50));
    paddle->SetPosition(Vector2(_w/2, _h - 100)); // DEBUGGING
    
    // Create the ball
    ball = new Ball(this);
    ball->SetPosition(Vector2(_w/2, _h - 150));
    
    
    ParseFile("Level.txt");
    
    float start_x = 64;
    float start_y = 48;
    
    Block* b;
    SpriteComponent* sc1;
    for (int i = 0; i < 15; i++) {
        for (int j = 0; j < 15; j++) {
            
            // Look for the file data that will be good for this block
            switch(fileData[i][j]) {
                case 'A':
                    // Load block A
                    b = new Block(this);
                    b->SetPosition(Vector2(start_x, start_y));
                    sc1 = new SpriteComponent(b);
                    sc1->SetTexture(GetTexture("Assets/BlockA.png"));
                    break;
                case 'B':
                    // Load block B
                    b = new Block(this);
                    b->SetPosition(Vector2(start_x, start_y));
                    sc1 = new SpriteComponent(b);
                    sc1->SetTexture(GetTexture("Assets/BlockB.png"));
                    break;
                case 'C':
                    // Load block C
                    b = new Block(this);
                    b->SetPosition(Vector2(start_x, start_y));
                    sc1 = new SpriteComponent(b);
                    sc1->SetTexture(GetTexture("Assets/BlockC.png"));
                    break;
                case 'D':
                    // Load block D
                    b = new Block(this);
                    b->SetPosition(Vector2(start_x, start_y));
                    sc1 = new SpriteComponent(b);
                    sc1->SetTexture(GetTexture("Assets/BlockD.png"));
                    break;
                case 'E':
                    // Load block E
                    b = new Block(this);
                    b->SetPosition(Vector2(start_x, start_y));
                    sc1 = new SpriteComponent(b);
                    sc1->SetTexture(GetTexture("Assets/BlockE.png"));
                    break;
                case 'F':
                    // Load block F
                    b = new Block(this);
                    b->SetPosition(Vector2(start_x, start_y));
                    sc1 = new SpriteComponent(b);
                    sc1->SetTexture(GetTexture("Assets/BlockF.png"));
                    break;
                default:
                    // Do nothing
                    break;
            }
            start_x += 64;
        }
        start_y += 32;
        start_x = 64;
    }
    
}

// Unload the data
void Game::UnloadData() {
    
    // Emptying out the actors and the sprites
    while (!actors.empty()) {
        delete actors.back();
    }
    
    for (auto it = sprites.begin(); it != sprites.end(); ++it) {
        SDL_DestroyTexture(it->second);
    }
     
    sprites.clear();
}

// Get the texture
SDL_Texture* Game::GetTexture(std::string imageName) {
    auto it = sprites.find(imageName);
    if (it != sprites.end()) {
        return it->second;
    }
    return NULL;
}

// Load the texture
void Game::LoadTexture(std::string imageName) {
    SDL_Texture* insText = nullptr;
    
    // Creates the texture
    SDL_Surface *tempTexture;
    tempTexture = IMG_Load(imageName.c_str());
    if(!tempTexture) {
        printf("IMG_Load: %s\n", IMG_GetError());
        return;
    }
    insText = SDL_CreateTextureFromSurface(sdlr, tempTexture);
    SDL_FreeSurface(tempTexture);
    sprites.insert(std::pair<std::string, SDL_Texture*>(imageName, insText));
}

// Add Sprites
void Game::AddSprite(SpriteComponent* sprite){
    mSprites.emplace_back(sprite);
    std::sort(mSprites.begin(), mSprites.end(), [](SpriteComponent* a, SpriteComponent* b) {
        return a->GetDrawOrder() < b->GetDrawOrder();
    });
}

// Remove sprites
void Game::RemoveSprite(SpriteComponent* sprite){
    auto iter = std::find(mSprites.begin(), mSprites.end(), sprite);
    mSprites.erase(iter);
}

// Parse the file
void Game::ParseFile(std::string filename) {
    std::string data;
    std::ifstream file("Assets/Level.txt");
    if (file.is_open()) {
        int ind = 0;
        while (getline(file, data)) {
            for (int i = 0; i < data.size(); i++) {
                fileData[ind][i] = data[i];
            }
            ind++;
        }
        file.close();
    }
    else {
        std::cout << "Could not open the file" << std::endl;
    }
}

void Game::AddBlock(class Block *b) {
    blocks.push_back(b);
}

void Game::RemoveBlock(class Block *b) {
    auto iter = std::find(blocks.begin(), blocks.end(), b);
    blocks.erase(iter);
}


