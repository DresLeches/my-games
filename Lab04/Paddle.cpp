//
//  Paddle.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/5/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Paddle.h"
#include "PaddleMove.h"
#include <iostream>

Paddle::Paddle(class Game* game)
    : Actor(game)
{
    mSprite = new SpriteComponent(this);
    mMove = new PaddleMove(this);
    mColl = new CollisionComponent(this);
    mColl->SetSize(104, 24);
    mSprite->SetTexture(game->GetTexture("Assets/Paddle.png"));
    SetSprite(mSprite);
    SetMoveComponent(mMove);
    SetCollComponent(mColl);
}

Paddle::~Paddle() {
    
}

void Paddle::ActorInput(const Uint8 *keyState) {
    float moveDir = 0.0f;
    if (keyState[SDL_SCANCODE_LEFT]) {
        moveDir = -200.0f;
    }
    else if (keyState[SDL_SCANCODE_RIGHT]) {
        moveDir  = 200.0f;
    }
    mMove->SetForwardSpeed(moveDir);
}
