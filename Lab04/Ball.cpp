//
//  Ball.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/6/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Ball.h"
#include "BallMove.h"

Ball::Ball(class Game* game)
    : Actor(game)
{
    mSprite = new SpriteComponent(this);
    mMove = new BallMove(this);
    mColl = new CollisionComponent(this);
    
    mColl->SetSize(10, 10);
    mSprite->SetTexture(game->GetTexture("Assets/Ball.png"));
    SetMoveComponent(mMove);
}

void Ball::UpdateActor(float deltaTime) {
    if (mPosition.y + 11 > 768) {
        SetPosition(Vector2(1024/2, 768 - 150));
        mMove->SetVelocity(Vector2(250, -250));
    }
}
