#include "HUD.h"
#include "Texture.h"
#include "Shader.h"
#include "Game.h"
#include "Renderer.h"
#include "Font.h"
#include <sstream>
#include <iomanip>
#include <string>
#include <iostream>


HUD::HUD(Game* game)
	:mGame(game)
	,mFont(nullptr)
    ,mHUDTimer(0)
    ,cpTextTimer(0.0f)
{
	// Load font
	mFont = new Font();
	mFont->Load("Assets/Inconsolata-Regular.ttf");
    mTimerText = mFont->RenderText("00:00.00");
    mCoinText = mFont->RenderText("00/55");
    mCPText = mFont->RenderText("");
}

HUD::~HUD()
{
	// Get rid of font
	if (mFont)
	{
		mFont->Unload();
		delete mFont;
	}
}


void HUD::Update(float deltaTime)
{
	// TODO
    mHUDTimer += deltaTime;
    cpTextTimer += deltaTime;
    mTimerText->Unload();
    mCoinText->Unload();
    delete mTimerText;
    delete mCoinText;
    
    double temp = mHUDTimer;
    std::string time = "";
    std::string strMin = "";
    std::string strSec = "";
    std::string strFrac = "";
    
    int min = static_cast<int>(temp / 60.0f);
    int sec = static_cast<int>(temp - (min * 60.0f));
    int frac = static_cast<int>((temp - (min * 60 + sec)) * 100);
    
    strMin = std::to_string(min);
    strSec = std::to_string(sec);
    strFrac = std::to_string(frac);
    
    if (min / 10 == 0) {
        strMin = "0" + strMin;
    }
    if (sec / 10 == 0) {
        strSec = "0" + strSec;
    }
    if (frac / 10 == 0) {
        strFrac = "0" + strFrac;
    }
    
    time = strMin + ":" + strSec + "." + strFrac;
    mTimerText = mFont->RenderText(time);
    
    std::string coins = std::to_string(mGame->GetCoinCounter()) + "/55";
    if (mGame->GetCoinCounter() / 10 == 0) {
        coins = "0" + coins;
    }
    mCoinText = mFont->RenderText(coins);
    
    std::cout << "Checkpoint Text Time: " << cpTextTimer << std::endl;
    std::cout << "Delta time: " << deltaTime << std::endl;
    if (cpTextTimer > 5.0f) {
        std::cout << "New One" << std::endl;
        delete mCPText;
        mCPText = mFont->RenderText("");
        cpTextTimer = 0.0f;
    }
    
}

void HUD::Draw(Shader* shader)
{
	// TODO
    DrawTexture(shader, mTimerText, Vector2(-428.0f, -325.0f));
    DrawTexture(shader, mCoinText, Vector2(-450.0f, -300.0f));
    if (mCPText) {
        DrawTexture(shader, mCPText, Vector2::Zero);
    }
}

void HUD::DrawTexture(class Shader* shader, class Texture* texture,
				 const Vector2& offset, float scale)
{
	// Scale the quad by the width/height of texture
	Matrix4 scaleMat = Matrix4::CreateScale(
		static_cast<float>(texture->GetWidth()) * scale,
		static_cast<float>(texture->GetHeight()) * scale,
		1.0f);
	// Translate to position on screen
	Matrix4 transMat = Matrix4::CreateTranslation(
		Vector3(offset.x, offset.y, 0.0f));	
	// Set world transform
	Matrix4 world = scaleMat * transMat;
	shader->SetMatrixUniform("uWorldTransform", world);
	// Set current texture
	texture->SetActive();
	// Draw quad
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
}

void HUD::UpdateCheckpointText(std::string checkPointTxt) {
    if (mCPText) {
        mCPText->Unload();
        delete mCPText;
    }
    mCPText = mFont->RenderText(checkPointTxt);
    cpTextTimer = 0.0f;
}
