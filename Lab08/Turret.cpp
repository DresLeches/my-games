//
//  Turret.cpp
//  Game-mac
//
//  Created by Pablo Chung on 3/14/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "Turret.h"
#include "Game.h"
#include "Mesh.h"
#include "MeshComponent.h"
#include "MoveComponent.h"

Turret::Turret(Game* game)
:Actor(game)
,mRotRightKey(SDL_SCANCODE_E)
,mRotLeftKey(SDL_SCANCODE_Q)
{
    Mesh* m = new Mesh();
    m->Load("Assets/TankTurret.gpmesh", mGame->GetRenderer());
    mMesh = new MeshComponent(this);
    mMesh->SetMesh(m);
    mMove = new MoveComponent(this);
    mScale = 1.8f;
}

Turret::~Turret() {  }

void Turret::SetPlayerTwo() {
    mRotRightKey = SDL_SCANCODE_P;
    mRotLeftKey = SDL_SCANCODE_I;
    mMesh->SetTextureIndex(1);
}

void Turret::ActorInput(const Uint8 *keyState) {
    if (keyState[mRotLeftKey]) {
        mMove->SetAngularSpeed(Math::TwoPi);
       // SetRotation(Math::TwoPi);
    }
    else if (keyState[mRotRightKey]) {
        //SetRotation(-Math::TwoPi);
        mMove->SetAngularSpeed(-Math::TwoPi);
    }
    else {
        mMove->SetAngularSpeed(0.0f);
    }
}
