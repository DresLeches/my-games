//
//  Tank.h
//  Game-mac
//
//  Created by Pablo Chung on 3/14/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Tank_h
#define Tank_h

#include "Actor.h"
#include "SDL/SDL.h"

class Tank : public Actor {
public:
    Tank(class Game* game);
    ~Tank();
    void UpdateActor(float deltaTime);
    void SetPlayerTwo();
    void Fire();
    void Respawn();
    void ActorInput(const Uint8 *keyState);
    void SetInitPosition(Vector3 pos) { initPos = pos; }
    void SetOpponet(Tank* t) { mOpponet = t; }
    Tank* GetOpponet() { return mOpponet; } 
private:
    class Turret* mTurret;
    Vector3 initPos;
    SDL_Scancode mFireKey;
    Tank* mOpponet;
    bool mFired;
};


#endif /* Tank_h */
