//
//  TankMove.cpp
//  Game-mac
//
//  Created by Pablo Chung on 3/14/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include "TankMove.h"
#include "Math.h"
#include "MoveComponent.h"
#include "Game.h"
#include "Actor.h"
#include "CollisionComponent.h"
#include "Block.h"
#include "Tank.h"

TankMove::TankMove(class Actor* owner)
:MoveComponent(owner)
,mForwardKey(SDL_SCANCODE_W)
,mBackKey(SDL_SCANCODE_S)
,mRotRightKey(SDL_SCANCODE_D)
,mRotLeftKey(SDL_SCANCODE_A)
{}

TankMove::~TankMove() { }

void TankMove::SetPlayerTwo() {
    mForwardKey = SDL_SCANCODE_O;
    mBackKey = SDL_SCANCODE_L;
    mRotRightKey = SDL_SCANCODE_SEMICOLON;
    mRotLeftKey = SDL_SCANCODE_K;
}

void TankMove::Update(float deltaTime) {
    MoveComponent::Update(deltaTime);
    Vector3 pos = mOwner->GetPosition();
    //pos += mOwner->GetForward() * GetForwardSpeed() * deltaTime;
    std::vector<Block*> blocks = *(mOwner->GetGame()->GetBlockVec());
    for (auto block : blocks) {
        if (mOwner->GetCollision()->Intersect(block->GetCollision())) {
            Vector3 aMin = mOwner->GetCollision()->GetMin();
            Vector3 aMax = mOwner->GetCollision()->GetMax();
            Vector3 bMin = block->GetCollision()->GetMin();
            Vector3 bMax = block->GetCollision()->GetMax();
            
            float dx1 = Math::Abs(aMax.x - bMin.x);
            float dx2 = Math::Abs(aMin.x - bMax.x);
            float dy1 = Math::Abs(aMax.y - bMin.y);
            float dy2 = Math::Abs(aMin.y - bMax.y);
            float minDiff = std::min({dx1, dx2, dy1, dy2});
            
            if (dx1 == minDiff) {
                // Left collision has occurred
                pos.x -= dx1;
            }
            else if (dx2 == minDiff) {
                // Right collision has occurred
                pos.x += dx2;
            }
            else if (dy1 == minDiff) {
                // Top collision has occurred
                pos.y -= dy1;
            }
            else if (dy2 == minDiff) {
                // Bottom collision has occurred
                pos.y += dy2;
            }
            mOwner->SetPosition(pos);
        }
    }
}

void TankMove::ProcessInput(const Uint8* keyState) {
    int speed = 0;
    if(keyState[mForwardKey]) {
        speed = 250;
    }
    else if (keyState[mBackKey]) {
        speed= -250;
    }
    SetForwardSpeed(speed);
    
    float rot = 0.0f;
    if (keyState[mRotLeftKey]) {
        rot = Math::TwoPi;
    }
    else if (keyState[mRotRightKey]) {
        rot = -Math::TwoPi;
    }
    SetAngularSpeed(rot);
    
}
