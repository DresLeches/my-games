//
//  Bullet.cpp
//  Game-mac
//
//  Created by Pablo Chung on 3/19/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "Bullet.h"
#include "Game.h"
#include "MoveComponent.h"
#include "Mesh.h"
#include "MeshComponent.h"
#include "CollisionComponent.h"
#include "Block.h"

Bullet::Bullet(class Game* game)
:Actor(game)
{
    mMove = new MoveComponent(this);
    Mesh* m = new Mesh();
    m->Load("Assets/Sphere.gpmesh", mGame->GetRenderer());
    mMesh = new MeshComponent(this);
    mMesh->SetMesh(m);
    mCollision = new CollisionComponent(this);
    mCollision->SetSize(10.0f, 10.0f, 10.0f);
    
    SetScale(0.5f);
    mMove->SetForwardSpeed(400.0f);
    
    
}

Bullet::~Bullet() {
    
}

void Bullet::UpdateActor(float deltaTime) {
    // Check if the bullet intersected with the block
    std::vector<Block*> blocks = *(GetGame()->GetBlockVec());
    for (auto block : blocks) {
        if (GetCollision()->Intersect(block->GetCollision())) {
            Vector3 aMin = GetCollision()->GetMin();
            Vector3 aMax = GetCollision()->GetMax();
            Vector3 bMin = block->GetCollision()->GetMin();
            Vector3 bMax = block->GetCollision()->GetMax();
            
            float dx1 = Math::Abs(aMax.x - bMin.x);
            float dx2 = Math::Abs(aMin.x - bMax.x);
            float dy1 = Math::Abs(aMax.y - bMin.y);
            float dy2 = Math::Abs(aMin.y - bMax.y);
            float minDiff = std::min({dx1, dx2, dy1, dy2});
            
            if (dx1 == minDiff || dx2 == minDiff || dy1 == minDiff || dy2 == minDiff) {
                SetState(Actor::EDead);
            }
        }
    }
    
    // Check if the bullet intersected with the tank
    if (GetCollision()->Intersect(tank->GetOpponet()->GetCollision())) {
        Vector3 aMin = GetCollision()->GetMin();
        Vector3 aMax = GetCollision()->GetMax();
        Vector3 bMin = tank->GetCollision()->GetMin();
        Vector3 bMax = tank->GetCollision()->GetMax();
        
        float dx1 = Math::Abs(aMax.x - bMin.x);
        float dx2 = Math::Abs(aMin.x - bMax.x);
        float dy1 = Math::Abs(aMax.y - bMin.y);
        float dy2 = Math::Abs(aMin.y - bMax.y);
        float minDiff = std::min({dx1, dx2, dy1, dy2});
        
        if (dx1 == minDiff || dx2 == minDiff || dy1 == minDiff || dy2 == minDiff) {
            SetState(Actor::EDead);
            tank->GetOpponet()->Respawn();
        }
    }
}


