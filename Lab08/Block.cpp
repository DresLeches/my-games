//
//  Block.cpp
//  Game-mac
//
//  Created by Pablo Chung on 3/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "Block.h"
#include "MeshComponent.h"
#include "Game.h"
#include "Renderer.h"
#include "CollisionComponent.h"

Block::Block(class Game* game)
:Actor(game)
{
    mMesh = new MeshComponent(this);
    mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Cube.gpmesh"));
    mScale = 64.0f;
    mCollision = new CollisionComponent(this);
    mCollision->SetSize(1.0f, 1.0f, 1.0f);
    mGame->GetBlockVec()->push_back(this);
}

Block::~Block() {
    mGame->GetBlockVec()->erase(std::find(mGame->GetBlockVec()->begin(), mGame->GetBlockVec()->end(), this));
}
