//
//  Turret.h
//  Game-mac
//
//  Created by Pablo Chung on 3/14/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Turret_h
#define Turret_h

//#include "Turret.h"
#include "Actor.h"
#include "SDL/SDL.h"

class Turret : public Actor {
public:
    Turret(class Game* game);
    ~Turret();
    void ActorInput(const Uint8 *keyState);
    void SetPlayerTwo();
private:
    SDL_Scancode mRotRightKey;
    SDL_Scancode mRotLeftKey;
};

#endif /* Turret_h */
