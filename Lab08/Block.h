//
//  Block.h
//  Game-mac
//
//  Created by Pablo Chung on 3/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Block_h
#define Block_h
#include "Actor.h"

class Block : public Actor {
public:
    Block(class Game* game);
    ~Block();
};

#endif /* Block_h */
