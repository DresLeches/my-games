#pragma once
#include "Component.h"
#include "SDL/SDL.h"

class MoveComponent : public Component
{
public:
	// Constructor
	// (the lower the update order, the earlier the component is updated)
    MoveComponent(class Actor* owner);
	// Destructor
	virtual ~MoveComponent();
	virtual void Update(float deltaTime) override;
    
    // Getters/setters
    float GetAngularSpeed() const { return mAngularSpeed; }
    float GetForwardSpeed() const { return mForwardSpeed; }
    void SetAngularSpeed(float speed) { mAngularSpeed = speed; }
    void SetForwardSpeed(float speed) { mForwardSpeed = speed; }

    // Angular speed (in radians/second)
    float mAngularSpeed;
    // Forward speed (in pixels/second)
    float mForwardSpeed;
};
