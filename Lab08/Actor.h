#pragma once
#include "Math.h"
#include "SDL/SDL_stdinc.h"
#include <vector>

class CollisionComponent;
class MoveComponent;
class SpriteComponent;
class MeshComponent;

class Actor
{
public:
	enum State
	{
		EActive,
		EPaused,
		EDead
	};
	
	Actor(class Game* game);
	virtual ~Actor();

    void ProcessInput(const Uint8 *keyState);
    virtual void ActorInput(const Uint8 *keyState) {}

	// Update function called from Game (not overridable)
	void Update(float deltaTime);
	// Any actor-specific update code (overridable)
	virtual void UpdateActor(float deltaTime);

	// Getters/setters
	const Vector3& GetPosition() const { return mPosition; }
	void SetPosition(const Vector3& pos) { mPosition = pos; }
	float GetScale() const { return mScale; }
	void SetScale(float scale) { mScale = scale; }
	float GetRotation() const { return mRotation; }
	void SetRotation(float rotation) { mRotation = rotation; }
    Vector3 GetForward() const;

	State GetState() const { return mState; }
	void SetState(State state) { mState = state; }

	class Game* GetGame() { return mGame; }
    MoveComponent* GetMove() { return mMove; }
    CollisionComponent* GetCollision() const { return mCollision; }
    const Matrix4& GetWorldTransform() const { return mWorldTransform; }

protected:
	// Actor's state
	State mState;

	// Transform
	Vector3 mPosition;
	float mScale;
	float mRotation;

	class Game* mGame;
    MoveComponent* mMove;
    CollisionComponent* mCollision;
    MeshComponent* mMesh;
    
    Matrix4 mWorldTransform;
};
