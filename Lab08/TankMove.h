//
//  TankMove.h
//  Game-mac
//
//  Created by Pablo Chung on 3/14/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef TankMove_h
#define TankMove_h
#include "MoveComponent.h"

class TankMove : public MoveComponent {
public:
    TankMove(class Actor* owner);
    ~TankMove();
    void Update(float deltaTime) override;
    void ProcessInput(const Uint8* keyState) override;
    void SetPlayerTwo();
private:
    SDL_Scancode mForwardKey;
    SDL_Scancode mBackKey;
    SDL_Scancode mRotRightKey;
    SDL_Scancode mRotLeftKey;
};

#endif /* TankMove_h */
