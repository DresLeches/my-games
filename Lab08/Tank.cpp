//
//  Tank.cpp
//  Game-mac
//
//  Created by Pablo Chung on 3/14/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "Tank.h"
#include "MeshComponent.h"
#include "Mesh.h"
#include "Game.h"
#include "MoveComponent.h"
#include "Turret.h"
#include "TankMove.h"
#include "CollisionComponent.h"
#include "Bullet.h"

Tank::Tank(Game* game)
:Actor(game)
,mFireKey(SDL_SCANCODE_LSHIFT)
,mFired(false)
{
    Mesh* m = new Mesh();
    m->Load("Assets/TankBase.gpmesh", mGame->GetRenderer());
    mMesh = new MeshComponent(this);
    mMesh->SetMesh(m);
    mMove = new TankMove(this);
    mTurret = new Turret(game);
    mCollision = new CollisionComponent(this);
    mCollision->SetSize(30.0f, 30.0f, 30.0f);
}

Tank::~Tank() { }

void Tank::SetPlayerTwo() {
    mTurret->SetPlayerTwo();
    TankMove* tempTankMove = new TankMove(this);
    mMove = tempTankMove;
    tempTankMove->SetPlayerTwo();
    mMesh->SetTextureIndex(1);
    mFireKey = SDL_SCANCODE_RSHIFT;
    
}

void Tank::Fire() {
    Bullet* b = new Bullet(mGame);
    b->SetPosition(GetPosition());
    b->SetRotation(mTurret->GetRotation());
    b->SetFiredFromTank(this);
}

void Tank::Respawn() {
    SetPosition(initPos);
    SetRotation(0.0f);
    mTurret->SetRotation(0.0f);
}

void Tank::UpdateActor(float deltaTime) {
    mTurret->SetPosition(GetPosition());
    
}

void Tank::ActorInput(const Uint8 *keyState) {
    if (!mFired && keyState[mFireKey]) {
        Fire();
    }
    mFired = keyState[mFireKey];
}
