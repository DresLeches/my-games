//
//  Bullet.h
//  Game-mac
//
//  Created by Pablo Chung on 3/19/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Bullet_h
#define Bullet_h
#include "Actor.h"
#include "Tank.h"

class Bullet : public Actor {
public:
    Bullet(class Game* game);
    ~Bullet();
    void SetFiredFromTank(Tank* t) { tank = t; }
    void UpdateActor(float deltaTime);
private:
    Tank* tank;
};


#endif /* Bullet_h */
