#include "Actor.h"
#include "CollisionComponent.h"
#include "Game.h"
#include "MoveComponent.h"
#include "MeshComponent.h"
#include <algorithm>

Actor::Actor(Game* game)
	: mState(EActive)
	, mPosition(Vector3::Zero)
	, mScale(1.0f)
	, mRotation(0.0f)
	, mGame(game)
    , mMove(nullptr)
    , mCollision(nullptr)
    , mMesh(nullptr)
{
	mGame->AddActor(this);
}

Actor::~Actor()
{
    delete mMove;
    delete mCollision;
    delete mMesh;
	mGame->RemoveActor(this);
}

void Actor::ProcessInput(const Uint8 *keyState)
{
    if (EActive == mState)
    {
        ActorInput(keyState);
        if (nullptr != mMove)
            mMove->ProcessInput(keyState);
        if (nullptr != mMesh)
            mMesh->ProcessInput(keyState);
    }
}

void Actor::Update(float deltaTime)
{
	if (EActive == mState)
	{
		
        if (nullptr != mMove)
            mMove->Update(deltaTime);
        if (nullptr != mMesh)
            mMesh->Update(deltaTime);
        
        UpdateActor(deltaTime);
        
        Matrix4 scaleMatrix;
        Matrix4 rotationMatrix;
        Matrix4 positionMatrix;
        
        scaleMatrix = Matrix4::CreateScale(mScale);
        rotationMatrix = Matrix4::CreateRotationZ(mRotation);
        positionMatrix = Matrix4::CreateTranslation(mPosition);
        mWorldTransform =  scaleMatrix * rotationMatrix * positionMatrix;
    }
    
}

void Actor::UpdateActor(float deltaTime)
{
}

Vector3 Actor::GetForward() const
{
    return Vector3(Math::Cos(mRotation), Math::Sin(mRotation), 0.0f);
}
