//
//  Plane.cpp
//  Game-mac
//
//  Created by Pablo Chung on 3/6/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "Plane.h"
#include "Game.h"
#include "Grid.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "PlaneMove.h"
#include "Tile.h"

Plane::Plane(class Game* game)
:Actor(game)
{
    mSprite = new SpriteComponent(this, 200);
    mSprite->SetTexture(mGame->GetTexture("Assets/Airplane.png"));
    mCollision = new CollisionComponent(this, 64, 64);
    
    Grid* temp = mGame->GetGrid();
    Vector2 pos = temp->GetStartTile()->GetPosition();
    SetPosition(pos);
    mMove = new PlaneMove(this);
    
    
    
    
    
    mGame->AddPlane(this);
}

Plane::~Plane() {
    mGame->RemovePlane(this);
}
