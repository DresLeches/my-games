#include "AnimatedSprite.h"
#include "Actor.h"
#include "Game.h"
#include "SDL/SDL_image.h"

AnimatedSprite::AnimatedSprite(Actor* owner, int drawOrder)
    : SpriteComponent(owner, drawOrder)
    , mAnimSpeed(15.0f)
    , mAnimTimer(0.0f)
{
}

void AnimatedSprite::Update(float deltaTime)
{
    SpriteComponent::Update(deltaTime);
    size_t frameCount = mImages.size();
    if (frameCount > 0)
    {
        mAnimTimer += mAnimSpeed * deltaTime;
        while (mAnimTimer >= frameCount)
            mAnimTimer -= frameCount;
        while (mAnimTimer < 0.0f)
            mAnimTimer += frameCount;
        SetTexture(mImages[(int)mAnimTimer]);
    }
}

void AnimatedSprite::AddImage(SDL_Texture* image)
{
    mImages.push_back(image);
}
