#include "Grid.h"
#include "Tile.h"
#include "Tower.h"
#include "Plane.h"
#include <SDL/SDL.h>
#include <algorithm>
#include <iostream>

Grid::Grid(class Game* game)
	:Actor(game)
	,mSelectedTile(nullptr)
    ,mNeedToBuild(false)
    ,spawnTimer(0.0f)
    ,nextSpawnTime(1.0f)
{
	// 7 rows, 16 columns
	mTiles.resize(NumRows);
	for (size_t i = 0; i < mTiles.size(); i++)
	{
		mTiles[i].resize(NumCols);
	}
	
	// Create tiles
	for (size_t i = 0; i < NumRows; i++)
	{
		for (size_t j = 0; j < NumCols; j++)
		{
			mTiles[i][j] = new Tile(GetGame());
			mTiles[i][j]->SetPosition(Vector2(TileSize/2.0f + j * TileSize, StartY + i * TileSize));
		}
	}
	
	// Set start/end tiles
	GetStartTile()->SetTileState(Tile::EStart);
	GetEndTile()->SetTileState(Tile::EBase);
	
	// Set up adjacency lists
	for (size_t i = 0; i < NumRows; i++)
	{
		for (size_t j = 0; j < NumCols; j++)
		{
			if (i > 0)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i-1][j]);
			}
			if (i < NumRows - 1)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i+1][j]);
			}
			if (j > 0)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i][j-1]);
			}
			if (j < NumCols - 1)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i][j+1]);
			}
		}
	}
    if (TryFindPath()) {
        UpdatePathTiles();
    }
}

void Grid::BuildTower(Tile* t) {
    if (!t->mBlocked) {
        t->mBlocked = true;
        if (TryFindPath()) {
            UpdatePathTiles();
            Tower *tower = new Tower(mGame);
            tower->SetPosition(t->GetPosition());
        }
        else {
            t->mBlocked = false;
            TryFindPath();
        }
    }
}

void Grid::BuildPlane() {
    new Plane(mGame);
}

void Grid::SelectTile(size_t row, size_t col)
{
	// Make sure it's a valid selection
	Tile::TileState tstate = mTiles[row][col]->GetTileState();
	if (tstate != Tile::EStart && tstate != Tile::EBase)
	{
		// Deselect previous one
		if (mSelectedTile)
		{
			mSelectedTile->ToggleSelect();
		}
		mSelectedTile = mTiles[row][col];
		mSelectedTile->ToggleSelect();
	}
}

Tile* Grid::GetStartTile()
{
	return mTiles[3][0];
}

Tile* Grid::GetEndTile()
{
	return mTiles[3][15];
}

void Grid::ActorInput(const Uint8 * keyState)
{
	// Process mouse click to select a tile
	int x, y;
	Uint32 buttons = SDL_GetMouseState(&x, &y);
	if (SDL_BUTTON(buttons) & SDL_BUTTON_LEFT)
	{
		// Calculate the x/y indices into the grid
		y -= static_cast<int>(StartY - TileSize / 2);
		if (y >= 0)
		{
			x /= static_cast<int>(TileSize);
			y /= static_cast<int>(TileSize);
			if (x >= 0 && x < static_cast<int>(NumCols) && y >= 0 && y < static_cast<int>(NumRows))
			{
				SelectTile(y, x);
			}
		}
	}
    else if (keyState[SDL_SCANCODE_SPACE] && mSelectedTile != nullptr) {
        mNeedToBuild = true;
        //BuildTower(mSelectedTile);
        //mNeedToBuild = false;
    }
}

void Grid::UpdateActor(float deltaTime)
{
    if (mNeedToBuild) {
        BuildTower(mSelectedTile);
        mNeedToBuild = false;
    }
    
    spawnTimer += deltaTime;
    if (spawnTimer >= nextSpawnTime) {
        BuildPlane();
        nextSpawnTime += 1.0f;
    }
     
    
}

void Grid::ResetTiles() {
    for (int i = 0; i < mTiles.size(); i++) {
        for (Tile* t : mTiles[i]) {
            t->mInClosedSet = false;
            t->g = 0.0f;
        }
    }
}

bool Grid::IsInOpenSet(Tile *target) {
    for (Tile* t : mOpenSet) {
        if (t == target) {
            return true;
        }
    }
    return false;
}

void Grid::ResetTileDefault() {
    Tile* start = GetStartTile();
    Tile* end = GetEndTile();
    for (int i = 0; i < mTiles.size(); i++) {
        for (int j = 0; j < mTiles[i].size(); j++) {
            if (mTiles[i][j] != start && mTiles[i][j] != end) {
                mTiles[i][j]->SetTileState(Tile::EDefault);
            }
        }
    }
}

void Grid::SetPath() {
    Tile* currNode = GetStartTile();
    Tile* end = GetEndTile();
    while (currNode->GetParent() != end) {
        currNode = currNode->GetParent();
        currNode->SetTileState(Tile::EPath);
    }
}

void Grid::UpdatePathTiles() {
    ResetTileDefault();
    SetPath();
}

void Grid::RemoveFromOpenSet(Tile *target) {
    mOpenSet.erase(std::find(mOpenSet.begin(), mOpenSet.end(), target));
}

float Grid::ComputeManhatten(Tile *node1, Tile *node2) {
    float res = 0.0f;
    /*
    size_t x_1 = 0, y_1 = 0;
    size_t x_2 = 0, y_2 = 0;
    GetTileLocation(node1, x_1, y_1);
    GetTileLocation(node2, x_2, y_2);
    
    float dx = Math::Abs(x_1 - x_2);
    float dy = Math::Abs(y_1 - y_2);
    res = TileSize * (dx + dy);
    //res = (dx + dy);
     */
    float dx = Math::Abs(node1->GetPosition().x - node2->GetPosition().x);
    float dy = Math::Abs(node1->GetPosition().y - node2->GetPosition().y);
    res = TileSize * (dx + dy);
    return res;
}

float Grid::ComputeNewG(Tile* t) {
    float res = 0.0f;
    Tile* start = GetStartTile();
    res = ComputeManhatten(start, t);
    return res;
}

float Grid::ComputeNewH(Tile* t) {
    float res = 0.0f;
    Tile* end = GetEndTile();
    res = ComputeManhatten(t, end);
    return res;
}

Tile* Grid::FindTileWithLowest_f() {
    //Tile* res = nullptr;
    /*
    float f_val = 0.0f;
    for (Tile* t : mOpenSet) {
        if (t->f > f_val) {
            res = t;
            f_val = t->f;
        }
    }
     */
    Tile* res = mOpenSet[0];
    float f_val = res->f;
    for (int i = 1; i < mOpenSet.size(); i++) {
        if (f_val > mOpenSet[i]->f) {
            res = mOpenSet[i];
            f_val = mOpenSet[i]->f;
        }
    }
    return res;
}

bool Grid::TryFindPath() {
    bool pathExists = true;
    ResetTiles();
    Tile* currTile = GetEndTile();
    currTile->mInClosedSet = true;
    do {
        for (Tile* t : currTile->mAdjacent) {
            if (t->mInClosedSet) {
                continue;
            }
            else if (IsInOpenSet(t)) {
                float new_g = ComputeNewG(t);
                if (new_g < t->g) {
                    t->SetParent(currTile);
                    t->g = new_g;
                    t->f = t->g + t->h;
                }
            }
            else if (!t->mBlocked) {
                t->SetParent(currTile);
                t->g = ComputeNewG(t);
                t->h = ComputeNewH(t);
                t->f = t->g + t->h;
                mOpenSet.push_back(t);
            }
        }
        if (mOpenSet.empty()) {
            pathExists = false;
            break;
        }
        currTile = FindTileWithLowest_f();
        RemoveFromOpenSet(currTile);
        currTile->mInClosedSet = true;
    }
    while(currTile != GetStartTile());
    while (!mOpenSet.empty()) {
        mOpenSet.pop_back();
    }
    return pathExists;
}

