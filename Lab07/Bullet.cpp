//
//  Bullet.cpp
//  Game-mac
//
//  Created by Pablo Chung on 3/7/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "Bullet.h"
#include "Game.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "MoveComponent.h"
#include "Plane.h"

Bullet::Bullet(class Game* game)
:Actor(game)
,timeAfterFired(0.0f)
{
    mSprite = new SpriteComponent(this, 200);
    mSprite->SetTexture(mGame->GetTexture("Assets/Bullet.png"));
    mCollision = new CollisionComponent(this, 10, 10);
    mMove = new MoveComponent(this);
    mMove->mFwdSpeed = 500.0f;
}

Bullet::~Bullet() { }

void Bullet::UpdateActor(float deltaTime) {
    timeAfterFired += deltaTime;
    if (timeAfterFired >= 2.0f) {
        SetState(Actor::EDead);
    }
    else {
        std::vector<Plane*> planes = mGame->GetPlanes();
        for (Plane* p : planes) {
            if (mCollision->Intersect(p->GetCollision())) {
                SetState(Actor::EDead);
                p->SetState(Actor::EDead);
                break;
            }
        }
    }
}


