#pragma once
#include "Component.h"
#include "Math.h"
#include "SDL/SDL.h"

class SpriteComponent : public Component
{
public:
	// Constructor
	// (the lower the update order, the earlier the component is updated)
	SpriteComponent(class Actor* owner, int drawOrder = 100);
	// Destructor
	virtual ~SpriteComponent();
	virtual void Draw(SDL_Renderer* pRenderer);
    void SetTexture(SDL_Texture* pTexture);
    void SetOffset(const Vector2& offset) { mOffset = offset; }
	int GetDrawOrder() const { return mDrawOrder; }
	int GetWidth() const { return mWidth; }
	int GetHeight() const { return mHeight; }

protected:
	SDL_Texture* mTexture;
	int mDrawOrder;
	int mWidth, mHeight;
    Vector2 mOffset;
};
