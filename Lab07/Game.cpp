//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
#include "Actor.h"
#include "Background.h"
//#include "BarrelSpawner.h"
//#include "Block.h"
//#include "Coin.h"
#include "MoveComponent.h"
//#include "Player.h"
#include "SpriteComponent.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_mixer.h"
//#include "Grid.h"
#include <string>
#include <fstream>
#include "Plane.h"
#include "Tower.h"


Game::Game()
:mWindow(nullptr)
,mRenderer(nullptr)
,mIsRunning(true)
{
}

bool Game::Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) != 0)
	{
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return false;
	}

	mWindow = SDL_CreateWindow("ITP Game", 100, 100, 1024, 768, 0);
	if (!mWindow)
	{
		SDL_Log("Failed to create window: %s", SDL_GetError());
		return false;
	}
	
	mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (!mRenderer)
	{
		SDL_Log("Failed to create renderer: %s", SDL_GetError());
		return false;
	}

    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) != 0)
    {
        SDL_Log("Failed to initialize audio %s", SDL_GetError());
        return false;
    }

	int flags = IMG_INIT_PNG;
	int initted = IMG_Init(flags);
	if ((initted&flags) != flags) 
	{
		SDL_Log("IMG_Init: Failed to init required jpg and png support!\n");
		SDL_Log("IMG_Init: %s\n", IMG_GetError());
		return false;
	}
	
	LoadData();

	mTicksCount = SDL_GetTicks();
	
	return true;
}

void Game::RunLoop()
{
	while (mIsRunning)
	{
		ProcessInput();
		UpdateGame();
		GenerateOutput();
	}
}

void Game::ProcessInput()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_QUIT:
				mIsRunning = false;
				break;
		}
	}
	
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if (state[SDL_SCANCODE_ESCAPE])
	{
		mIsRunning = false;
	}

    for (Actor* pActor : mActors)
    {
        pActor->ProcessInput(state);
    }
}

void Game::UpdateGame()
{
	// Compute delta time
	// Wait until 16ms has elapsed since last frame
	while (!SDL_TICKS_PASSED(SDL_GetTicks(), mTicksCount + 16))
		;

	float deltaTime = (SDL_GetTicks() - mTicksCount) / 1000.0f;
	if (deltaTime > 0.05f)
	{
		deltaTime = 0.05f;
	}
	mTicksCount = SDL_GetTicks();

	auto copy = mActors;
	for (Actor* pActor : copy)
	{
		pActor->Update(deltaTime);
	}
	std::vector<Actor*> deadList;
	for (Actor* pActor : mActors)
	{
		if (Actor::EDead == pActor->GetState())
			deadList.push_back(pActor);
	}
	for (Actor* pActor : deadList)
	{
		delete pActor;
	}
}

void Game::GenerateOutput()
{
	SDL_SetRenderDrawColor(mRenderer, 34, 139, 34, 255);
	SDL_RenderClear(mRenderer);
	
    for (auto sprite : mSprites)
        sprite->Draw(mRenderer);

	SDL_RenderPresent(mRenderer);
}

void Game::Shutdown()
{
	UnloadData();
	IMG_Quit();
	SDL_DestroyRenderer(mRenderer);
	SDL_DestroyWindow(mWindow);
    Mix_CloseAudio();
	SDL_Quit();
}

void Game::AddActor(Actor *pActor)
{
	mActors.push_back(pActor);
}

void Game::RemoveActor(Actor *pActor)
{
	auto it = mActors.begin();
	while (it != mActors.end())
	{
		if (*it == pActor)
		{
			mActors.erase(it);
			return;
		}
		++it;
	}
}

void Game::AddSprite(SpriteComponent* pSprite)
{
  auto it = mSprites.begin();
  while (it != mSprites.end())
  {
    if ((*it)->GetDrawOrder() > pSprite->GetDrawOrder())
    {
      mSprites.insert(it, pSprite);
      return;
    }
    ++it;
  }
  mSprites.push_back(pSprite);
}

void Game::RemoveSprite(SpriteComponent* pSprite)
{
  auto it = mSprites.begin();
  while (it != mSprites.end())
  {
    if ((*it) == pSprite)
    {
      mSprites.erase(it);
      return;
    }
    ++it;
  }
}

void Game::RemoveBlock(Block *pBlock)
{
    /*
    auto it = mBlocks.begin();
    while (it != mBlocks.end())
    {
        if ((*it) == pBlock)
        {
            mBlocks.erase(it);
            return;
        }
        ++it;
    }
     */
}

SDL_Texture* Game::LoadTexture(const std::string& filename)
{
	SDL_Texture* pTex = GetTexture(filename);
	if (nullptr == pTex)
	{
		SDL_Surface* pSurface = IMG_Load(filename.c_str());
		if (nullptr != pSurface)
		{
			pTex = SDL_CreateTextureFromSurface(mRenderer, pSurface);
			if (nullptr != pTex)
				mTextureCache[filename] = pTex;
		}
	}
	return pTex;
}

SDL_Texture* Game::GetTexture(const std::string& filename)
{
	auto find = mTextureCache.find(filename);
	if (find != mTextureCache.end())
		return find->second;
	return nullptr;
}

Mix_Chunk* Game::LoadSound(const std::string& filename)
{
    Mix_Chunk* pChunk = GetSound(filename);
    if (nullptr == pChunk)
    {
        pChunk = Mix_LoadWAV(filename.c_str());
        if (nullptr != pChunk)
            mSoundCache[filename] = pChunk;
    }
    return pChunk;
}

Mix_Chunk* Game::GetSound(const std::string& filename)
{
    auto find = mSoundCache.find(filename);
    if (find != mSoundCache.end())
        return find->second;
    return nullptr;
}

void Game::LoadData()
{
    LoadTexture("Assets/Airplane.png");
    LoadTexture("Assets/Bullet.png");
    LoadTexture("Assets/TileBrown.png");
    LoadTexture("Assets/TileBrownSelected.png");
    LoadTexture("Assets/TileGreen.png");
    LoadTexture("Assets/TileGrey.png");
    LoadTexture("Assets/TileGreySelected.png");
    LoadTexture("Assets/TileTan.png");
    LoadTexture("Assets/Tower.png");
    
    grid = new Grid(this);
    
    
    /*
#if 0
    {
        Actor *pActor = new Actor(this);
        SpriteComponent *pSprite = new SpriteComponent(pActor, 10);
        pSprite->SetTexture(LoadTexture("Assets/Background.png"));
        pActor->SetPosition(Vector2(512.0f, 384.0f));
    }
#else
    {   // load the sky
        float x = -512.0f;
        Actor *pActor = new Actor(this);
        Background *pSprite = new Background(pActor, 10);
        pSprite->mParallax = 0.25f;
        for (int i = 0; i < 3; ++i)
        {
            std::string filename = "Assets/Background/Sky_" + std::to_string(i) + ".png";
            pSprite->AddImage(LoadTexture(filename));
        }
        pActor->SetPosition(Vector2(x, 0.0f));
    }
    {   // load the mid
        float x = -512.0f;
        Actor *pActor = new Actor(this);
        Background *pSprite = new Background(pActor, 20);
        pSprite->mParallax = 0.5f;
        for (int i = 0; i < 3; ++i)
        {
            std::string filename = "Assets/Background/Mid_" + std::to_string(i) + ".png";
            pSprite->AddImage(LoadTexture(filename));
        }
        pActor->SetPosition(Vector2(x, 0.0f));
    }
    {   // load the foreground
        float x = -512.0f;
        Actor *pActor = new Actor(this);
        Background *pSprite = new Background(pActor, 30);
        pSprite->mParallax = 0.75f;
        for (int i = 0; i < 3; ++i)
        {
            std::string filename = "Assets/Background/Fore_" + std::to_string(i) + ".png";
            pSprite->AddImage(LoadTexture(filename));
        }
        pActor->SetPosition(Vector2(x, 0.0f));
    }
#endif
    //LoadNextLevel();
     */
}

static float s_levelX = 0.0f;
static int s_levelSegment = 0;
void Game::LoadNextLevel()
{
    const int s_levelHeight = 24;
    const int s_levelWidth = 56;

    std::string levelFile = "Assets/Level" + std::to_string(s_levelSegment) + ".txt";
    std::ifstream file(levelFile);
    if (file.is_open())
    {
        for (int y = 0; y < s_levelHeight; ++y)
        {
            std::string buffer;
            std::getline(file, buffer);
            Vector2 start(32.0f + s_levelX, 16.0f);
            for (int x = 0; x < s_levelWidth; ++x)
            {
                switch (buffer[x])
                {
                    case '.':
                        break;
                    case 'P':
                        /*
                        if (nullptr == mPlayer)
                        {
                            mPlayer = new Player(this);
                            mPlayer->SetPosition(start + Vector2(x * 64.0f, y * 32.0f + 16.0f));
                        }
                         */
                        break;
                    case 'O':
                    {
                        /*
                        BarrelSpawner *pBarrel = new BarrelSpawner(this);
                        pBarrel->SetPosition(start + Vector2(x * 64.0f, y * 32.0f + 16.0f));
                         */
                    }
                        break;
#if 1
                    case '*':
                    {
                        /*
                        Coin *pCoin = new Coin(this);
                        pCoin->SetPosition(start + Vector2(x * 64.0f, y * 32.0f - 16.0f));
                         */
                    }
                        break;
#endif
                    case 'A':
                    case 'B':
                    case 'C':
                    case 'D':
                    case 'E':
                    {
                        /*
                        Block *pBlock = new Block(this);
                        pBlock->SetPosition(start + Vector2(x * 64.0f, y * 32.0f));
                        pBlock->SetStyle(buffer[x] - 'A');
                        mBlocks.push_back(pBlock);
                         */
                    }
                        break;
                }
            }
        }
    }
    s_levelX += s_levelWidth * 64.0f;
    s_levelSegment += 1;
    if (s_levelSegment > 3)
        s_levelSegment = 0;
}

void Game::UnloadData()
{
	while (false == mActors.empty())
		delete mActors.back();
	for (auto texIt : mTextureCache)
	{
		SDL_DestroyTexture(texIt.second);
	}
	mTextureCache.clear();
    for (auto soundIt : mSoundCache)
    {
        Mix_FreeChunk(soundIt.second);
    }
    mSoundCache.clear();
}

void Game::AddPlane(class Plane *plane) {
    mPlanes.push_back(plane);
}

void Game::RemovePlane(class Plane *p) {
    mPlanes.erase(std::find(mPlanes.begin(), mPlanes.end(), p));
}

Plane* Game::FindClosestPlane(class Tower* t) {
    if (mPlanes.empty()) {
        return nullptr;
    }
    
    Plane* p = mPlanes[0];
    Vector2 vec = p->GetPosition() - t->GetPosition();
    float shortestDist = pow(vec.x, 2) + pow(vec.y, 2);
    for (int i = 1; i < mPlanes.size(); i++) {
        Vector2 tempVec = mPlanes[i]->GetPosition() - t->GetPosition();
        float tempDist = pow(tempVec.x, 2) + pow(tempVec.y, 2);
        
        if (tempDist < shortestDist) {
            p = mPlanes[i];
            shortestDist = tempDist;
        }
    }
    return p;
    
}
