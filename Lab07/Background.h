#pragma once
#include "SpriteComponent.h"
#include "SDL/SDL.h"
#include <vector>

class Background : public SpriteComponent
{
public:
    // Constructor
    // (the lower the update order, the earlier the component is updated)
    Background(class Actor* owner, int drawOrder = 100);
    
    void Draw(SDL_Renderer* pRenderer) override;
    void AddImage(SDL_Texture* image);
    
    float mParallax;
    
protected:
    std::vector<SDL_Texture*> mImages;
};
