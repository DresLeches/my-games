#pragma once
#include "Actor.h"
#include <vector>

class Grid : public Actor
{
public:
	Grid(class Game* game);
	// Getters for start/end tiles
	class Tile* GetStartTile();
	class Tile* GetEndTile();

	// Overrides from base
	void ActorInput(const Uint8* keyState) override;
	void UpdateActor(float deltaTime) override;
    bool TryFindPath();
    void ResetTiles();
    void GetTileLocation(Tile* t, size_t& row, size_t& col);
    bool IsInOpenSet(Tile* t);
    float ComputeNewG(Tile* t);
    float ComputeNewH(Tile* t);
    float ComputeManhatten(Tile* node1, Tile* node2);
    Tile* FindTileWithLowest_f();
    void RemoveFromOpenSet(Tile *target);
    void UpdatePathTiles();
    void ResetTileDefault();
    void SetPath();
    void BuildTower(Tile* t);
    void BuildPlane();
    const float GetTileSize() { return TileSize; }
private:
	// Select the tile in the specified row/column
	void SelectTile(size_t row, size_t col);
	// Tile that's currently selected (nullptr if none)
	class Tile* mSelectedTile;
	// 2D grid of actual tiles
	std::vector<std::vector<class Tile*>> mTiles;
    // Open set
    std::vector<class Tile*> mOpenSet;
    // Need to build this boolean
    bool mNeedToBuild;
    // 

	// Constants to adjust grid's properties
	const size_t NumRows = 7;
	const size_t NumCols = 16;
	const float StartY = 192.0f;
	const float TileSize = 64.0f;
    
    // Float timer
    float spawnTimer;
    float nextSpawnTime;
};
