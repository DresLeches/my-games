#include "SpriteComponent.h"
#include "Actor.h"
#include "Game.h"
#include "SDL/SDL_image.h"

SpriteComponent::SpriteComponent(Actor* owner, int drawOrder)
    : Component(owner)
    , mTexture(nullptr)
    , mDrawOrder(drawOrder)
    , mWidth(0)
    , mHeight(0)
{
    mOwner->GetGame()->AddSprite(this);
}

SpriteComponent::~SpriteComponent()
{
    mOwner->GetGame()->RemoveSprite(this);
}

void SpriteComponent::Draw(SDL_Renderer* pRenderer)
{
    SDL_Rect rect;
    rect.w = static_cast<int>(mWidth * mOwner->GetScale());
    rect.h = static_cast<int>(mHeight * mOwner->GetScale());
    Vector2 pos = mOwner->GetPosition() + mOffset;
    rect.x = static_cast<int>(pos.x - mOwner->GetGame()->GetCameraPos().x);
    rect.y = static_cast<int>(pos.y - mOwner->GetGame()->GetCameraPos().y);
    double ang = -Math::ToDegrees(mOwner->GetRotation());
    SDL_RenderCopyEx(pRenderer, mTexture, nullptr, &rect, ang, nullptr, SDL_FLIP_NONE);
}

void SpriteComponent::SetTexture(SDL_Texture* pTexture) 
{
    mTexture = pTexture; 
    Uint32 format;
    int access;
    SDL_QueryTexture(mTexture, &format, &access, &mWidth, &mHeight);
    mOffset.x = -0.5f * mWidth;
    mOffset.y = -0.5f * mHeight;
}
