#pragma once
#include "Actor.h"
#include "Component.h"
#include "Math.h"

class CollisionComponent : public Component
{
public:
	// Constructor
    CollisionComponent(class Actor* owner, float width, float height)
        : Component(owner)
        , mWidth(width)
        , mHeight(height)
    {}
    const Vector2& GetCenter() const { return mOwner->GetPosition(); }
    float GetWidth() const { return mWidth; }
    float GetHeight() const { return mHeight; }

    bool Intersect(const CollisionComponent* other) const
    {
        Vector2 minA = GetCenter() - 0.5f * Vector2(GetWidth(), GetHeight());
        Vector2 minB = other->GetCenter() - 0.5f * Vector2(other->GetWidth(), other->GetHeight());
        Vector2 maxA = GetCenter() + 0.5f * Vector2(GetWidth(), GetHeight());
        Vector2 maxB = other->GetCenter() + 0.5f * Vector2(other->GetWidth(), other->GetHeight());
        if ((maxA.x < minB.x)
            || (minA.x > maxB.x)
            || (maxA.y < minB.y)
            || (minA.y > maxB.y)
            )
            return false;
        return true;
    }

private:
    float mWidth, mHeight;
};
