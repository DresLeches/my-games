#pragma once
#include "Component.h"
#include "SDL/SDL.h"

class MoveComponent : public Component
{
public:
	// Constructor
	// (the lower the update order, the earlier the component is updated)
    MoveComponent(class Actor* owner);
	// Destructor
	virtual ~MoveComponent();
	virtual void Update(float deltaTime) override;

    float mFwdSpeed;
    float mRotSpeed;
};
