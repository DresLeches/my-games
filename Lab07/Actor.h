#pragma once
#include "Math.h"
#include "SDL/SDL_stdinc.h"
#include <vector>

class CollisionComponent;
class MoveComponent;
class SpriteComponent;

class Actor
{
public:
	enum State
	{
		EActive,
		EPaused,
		EDead
	};
	
	Actor(class Game* game);
	virtual ~Actor();

    void ProcessInput(const Uint8 *keyState);
    virtual void ActorInput(const Uint8 *keyState) {}

	// Update function called from Game (not overridable)
	void Update(float deltaTime);
	// Any actor-specific update code (overridable)
	virtual void UpdateActor(float deltaTime);

	// Getters/setters
	const Vector2& GetPosition() const { return mPosition; }
	void SetPosition(const Vector2& pos) { mPosition = pos; }
	float GetScale() const { return mScale; }
	void SetScale(float scale) { mScale = scale; }
	float GetRotation() const { return mRotation; }
	void SetRotation(float rotation) { mRotation = rotation; }
    Vector2 GetForward() const;

	State GetState() const { return mState; }
	void SetState(State state) { mState = state; }

	class Game* GetGame() { return mGame; }
    SpriteComponent* GetSprite() { return mSprite; }
    MoveComponent* GetMove() { return mMove; }
    const CollisionComponent* GetCollision() const { return mCollision; }

protected:
	// Actor's state
	State mState;

	// Transform
	Vector2 mPosition;
	float mScale;
	float mRotation;

	class Game* mGame;
    SpriteComponent* mSprite;
    MoveComponent* mMove;
    CollisionComponent* mCollision;
};
