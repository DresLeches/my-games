#pragma once
#include "SDL/SDL_stdinc.h"

class Component
{
public:
	// Constructor
	// (the lower the update order, the earlier the component is updated)
	Component(class Actor* owner);
	// Destructor
	virtual ~Component();
	// Update this component by delta time
	virtual void Update(float deltaTime);
    virtual void ProcessInput(const Uint8 *keyState);

protected:
	// Owning actor
	class Actor* mOwner;
};
