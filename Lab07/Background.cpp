#include "Background.h"
#include "Actor.h"
#include "Game.h"
#include "SDL/SDL_image.h"

Background::Background(Actor* owner, int drawOrder)
: SpriteComponent(owner, drawOrder)
, mParallax(1.0f)
{
}

void Background::Draw(SDL_Renderer* pRenderer)
{
    double ang = -Math::ToDegrees(mOwner->GetRotation());
    Vector2 pos = mOwner->GetPosition() + mOffset - mParallax * mOwner->GetGame()->GetCameraPos();
#if 0
    while (pos.x < 0.0f)
        pos.x += mWidth;
    while (pos.x > 0.0f)
        pos.x -= mWidth;
    mOwner->SetPosition(pos - mOffset + mParallax * mOwner->GetGame()->GetCameraPos());
#endif

    int i = 0;
    while (pos.x < 1024.0f)
    {
        SDL_Texture* tex = mImages[i];
        Uint32 format;
        int access;
        int width, height;
        SDL_QueryTexture(tex, &format, &access, &width, &height);

        SDL_Rect rect;
        rect.w = static_cast<int>(width * mOwner->GetScale());
        rect.h = static_cast<int>(height * mOwner->GetScale());
        rect.x = static_cast<int>(pos.x);
        rect.y = static_cast<int>(pos.y);
        
        SDL_RenderCopyEx(pRenderer, tex, nullptr, &rect, ang, nullptr, SDL_FLIP_NONE);
        pos.x += width;
        
        ++i;
        if (i > 2)
            i = 0;
    }
}

void Background::AddImage(SDL_Texture* image)
{
    mImages.push_back(image);
    Uint32 format;
    int access;
    int width, height;
    SDL_QueryTexture(image, &format, &access, &width, &height);
    mWidth += width;
    mHeight = Math::Max(mHeight, height);
}


