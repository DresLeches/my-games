//
//  Bullet.h
//  Game-mac
//
//  Created by Pablo Chung on 3/7/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Bullet_h
#define Bullet_h
#include "Actor.h"
class Bullet : public Actor {
public:
    Bullet(class Game* game);
    ~Bullet();
    void UpdateActor(float deltaTime);
private:
    float timeAfterFired;
};

#endif /* Bullet_h */
