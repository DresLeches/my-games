//
//  PlaneMove.h
//  Game-mac
//
//  Created by Pablo Chung on 3/6/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef PlaneMove_h
#define PlaneMove_h
#include "MoveComponent.h"

class PlaneMove : public MoveComponent {
public:
    PlaneMove(Actor* owner);
    ~PlaneMove();
    void Update(float deltaTime);
    void SetNextTile(const class Tile* tile);
private:
    const class Tile* mNextTile;
};

#endif /* PlaneMove_h */
