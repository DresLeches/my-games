//
//  Tower.h
//  Game-mac
//
//  Created by Pablo Chung on 3/6/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Tower_h
#define Tower_h
#include "Actor.h"

class Tower : public Actor {
public:
    Tower(class Game* game);
    ~Tower();
    void UpdateActor(float deltaTime);
    void RotateTowerToPlane(class Plane *p);
private:
    float attackTimer;
    float nextAttackTime;
};

#endif /* Tower_h */
