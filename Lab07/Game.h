#pragma once
#include "SDL/SDL.h"
#include "Math.h"
#include <unordered_map>
#include <string>
#include <vector>
#include "Grid.h"

class Actor;
class Player;
class Block;
class SpriteComponent;
struct Mix_Chunk;

class Game
{
public:
	Game();
	bool Initialize();
	void RunLoop();
	void Shutdown();

	void AddActor(Actor* pActor);
	void RemoveActor(Actor* pActor);

    void AddSprite(SpriteComponent* pSprite);
    void RemoveSprite(SpriteComponent* pSprite);

	SDL_Texture* LoadTexture(const std::string& filename);
	SDL_Texture* GetTexture(const std::string& filename);

    Mix_Chunk* LoadSound(const std::string& filename);
    Mix_Chunk* GetSound(const std::string& filename);

    //Player* GetPlayer() { return mPlayer; }
    //const std::vector<Block*> GetBlocks() const { return mBlocks; }
    void RemoveBlock(Block* pBlock);

    const Vector2& GetCameraPos() const { return mCameraPos; }
    void SetCameraPos(const Vector2& cameraPos) { mCameraPos = cameraPos; }

    void LoadNextLevel();
    
    Grid* GetGrid() const { return grid; }
    
    void AddPlane(class Plane* p);
    void RemovePlane(class Plane* p);
    Plane* FindClosestPlane(class Tower* t);
    std::vector<Plane*> GetPlanes() { return mPlanes; }

private:
	void ProcessInput();
	void UpdateGame();
	void GenerateOutput();

	void LoadData();
	void UnloadData();

	SDL_Window* mWindow;
	SDL_Renderer* mRenderer;
	Uint32 mTicksCount;
	bool mIsRunning;

	std::vector<Actor*> mActors;
    std::vector<SpriteComponent*> mSprites;
    std::unordered_map<std::string, SDL_Texture*> mTextureCache;
    std::unordered_map<std::string, Mix_Chunk*> mSoundCache;

    //std::vector<Block*> mBlocks;
    class Grid* grid;

    //Player* mPlayer;
    Vector2 mCameraPos;
    
    // Plane vector
    std::vector<class Plane*> mPlanes;
};
