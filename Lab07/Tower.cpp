//
//  Tower.cpp
//  Game-mac
//
//  Created by Pablo Chung on 3/6/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "Tower.h"
#include "Game.h"
#include "SpriteComponent.h"
#include "Plane.h"
#include "Bullet.h"

Tower::Tower(Game* game)
:Actor(game)
,attackTimer(0.0f)
,nextAttackTime(2.0f)
{
    mSprite = new SpriteComponent(this, 200);
    mSprite->SetTexture(mGame->GetTexture("Assets/Tower.png"));
}

Tower::~Tower() {
}

void Tower::RotateTowerToPlane(Plane *p) {
    const float tileSize = mGame->GetGrid()->GetTileSize();
    Vector2 squareCenter(tileSize/2, tileSize/2);
    Vector2 planeVec = p->GetPosition() + squareCenter;
    Vector2 towerVec = GetPosition() + squareCenter;
    
    Vector2 vectorTP = planeVec - towerVec;
    float towerRotation = GetRotation();
    float rotationToPlane = Math::Atan2(-vectorTP.y, vectorTP.x);
    float deltaRotation = rotationToPlane - towerRotation;
    SetRotation(GetRotation() + deltaRotation);
}

void Tower::UpdateActor(float deltaTime) {
    attackTimer += deltaTime;
    if (attackTimer >= nextAttackTime) {
        Plane* p = mGame->FindClosestPlane(this);
        Vector2 vec = p->GetPosition() - GetPosition();
        float shortestDist = pow(vec.x, 2) + pow(vec.y, 2);
        shortestDist = Math::Sqrt(shortestDist);
        if (shortestDist <= 100.0f) {
            RotateTowerToPlane(p);
            Bullet* b = new Bullet(mGame);
            b->SetRotation(GetRotation());
            b->SetPosition(GetPosition());
            nextAttackTime += 2.0f;
        }
        
    }
}
