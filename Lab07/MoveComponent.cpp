#include "MoveComponent.h"
#include "Actor.h"
#include "Game.h"

MoveComponent::MoveComponent(Actor* owner)
    : Component(owner)
    , mFwdSpeed(0.0f)
    , mRotSpeed(0.0f)
{
}

MoveComponent::~MoveComponent()
{
}

void MoveComponent::Update(float deltaTime)
{
    float ang = mOwner->GetRotation() + mRotSpeed * deltaTime;
    mOwner->SetRotation(ang);
    Vector2 pos = mOwner->GetPosition() + static_cast<int>(mFwdSpeed * deltaTime) * mOwner->GetForward();
    /*
    if (pos.x > 1024.0f)
        pos.x -= 1024.0f;
    else if (pos.x < 0.0f)
        pos.x += 1024.0f;
    if (pos.y > 768.0f)
        pos.y -= 768.0f;
    else if (pos.y < 0.0f)
        pos.y += 768.0f;
     */
    mOwner->SetPosition(pos);
}


