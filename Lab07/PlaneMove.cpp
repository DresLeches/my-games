//
//  PlaneMove.cpp
//  Game-mac
//
//  Created by Pablo Chung on 3/6/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "PlaneMove.h"
#include "Tile.h"
#include "Grid.h"
#include "Game.h"

PlaneMove::PlaneMove(Actor* owner)
:MoveComponent(owner)
,mNextTile(nullptr)
{
    mFwdSpeed = 200.0f;
    SetNextTile(mOwner->GetGame()->GetGrid()->GetStartTile()->GetParent());
}

PlaneMove::~PlaneMove() { }

void PlaneMove::Update(float deltaTime) {
    MoveComponent::Update(deltaTime);
    if (mNextTile != nullptr) {
        const float tileSize = mOwner->GetGame()->GetGrid()->GetTileSize();
        Vector2 squareCenter(tileSize/2, tileSize/2);
        Vector2 planeVec = mOwner->GetPosition() + squareCenter;
        Vector2 tileVec = mNextTile->GetPosition() + squareCenter;
        
        Vector2 vectorPT = tileVec - planeVec;
        float dSquared = pow(vectorPT.x, 2) + pow(vectorPT.y, 2);
        float d = Math::Sqrt(dSquared);
        if (d <= 5.0f) {
            SetNextTile(mNextTile->GetParent());
        }
        if (vectorPT.x > 1024) {
            mOwner->SetState(Actor::EDead);
        }
    }
}

void PlaneMove::SetNextTile(const class Tile *tile) {
    mNextTile = tile;
    if (mNextTile == nullptr) {
        mOwner->SetRotation(0.0f);
    }
    else {
        const float tileSize = mOwner->GetGame()->GetGrid()->GetTileSize();
        Vector2 squareCenter(tileSize/2, tileSize/2);
        //Vector2 planeVec = mOwner->GetPosition();
        Vector2 planeVec = mOwner->GetPosition() + squareCenter;
        Vector2 tileVec = mNextTile->GetPosition() + squareCenter;
        
        Vector2 vectorPT = tileVec - planeVec;
        float planeRotation = mOwner->GetRotation();
        float rotationToTile = Math::Atan2(-vectorPT.y, vectorPT.x);
        float deltaRotation = rotationToTile - planeRotation;
        mOwner->SetRotation(mOwner->GetRotation() + deltaRotation);
    }
}
