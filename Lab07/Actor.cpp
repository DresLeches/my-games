#include "Actor.h"
#include "CollisionComponent.h"
#include "Game.h"
#include "MoveComponent.h"
#include "SpriteComponent.h"
#include <algorithm>

Actor::Actor(Game* game)
	: mState(EActive)
	, mPosition(Vector2::Zero)
	, mScale(1.0f)
	, mRotation(0.0f)
	, mGame(game)
    , mSprite(nullptr)
    , mMove(nullptr)
    , mCollision(nullptr)
{
	mGame->AddActor(this);
}

Actor::~Actor()
{
    delete mSprite;
    delete mMove;
    delete mCollision;
	mGame->RemoveActor(this);
}

void Actor::ProcessInput(const Uint8 *keyState)
{
    if (EActive == mState)
    {
        ActorInput(keyState);
        if (nullptr != mMove)
            mMove->ProcessInput(keyState);
    }
}

void Actor::Update(float deltaTime)
{
	if (EActive == mState)
	{
		UpdateActor(deltaTime);
        if (nullptr != mMove)
            mMove->Update(deltaTime);
        if (nullptr != mSprite)
            mSprite->Update(deltaTime);
    }
}

void Actor::UpdateActor(float deltaTime)
{
}

Vector2 Actor::GetForward() const
{
    return Vector2(Math::Cos(mRotation), -Math::Sin(mRotation));
}
