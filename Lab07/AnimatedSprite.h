#pragma once
#include "SpriteComponent.h"
#include "Math.h"
#include "SDL/SDL.h"
#include <vector>

class AnimatedSprite : public SpriteComponent
{
public:
	// Constructor
	// (the lower the update order, the earlier the component is updated)
    AnimatedSprite(class Actor* owner, int drawOrder = 100);
    void Update(float deltaTime) override;

    void AddImage(SDL_Texture* image);
    float mAnimSpeed;
    
protected:
    std::vector<SDL_Texture*> mImages;
    float mAnimTimer;
};
