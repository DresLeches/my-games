//
//  Coin.h
//  Game-mac
//
//  Created by Pablo Chung on 2/28/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Coin_h
#define Coin_h

#include "Actor.h"
#include "AnimatedSprite.h"

class Coin : public Actor {
public:
    Coin(class Game* game);
    ~Coin();
    void UpdateActor(float deltaTime);
private:
    // The offset when the position of the camera overtakes the position of the coin
    // Supposed to help make the game look smoother
    int offSet;
};

#endif /* Coin_h */
