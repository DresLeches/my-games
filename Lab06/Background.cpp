//
//  Background.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/22/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "Background.h"
#include "Actor.h"
#include "Game.h"

Background::Background(Actor* owner, int drawOrder)
: SpriteComponent(owner, drawOrder), mTextWidth(0), mTextHeight(0), mParallax(1.0f)
{
}

Background::~Background() { }

void Background::AddImage(SDL_Texture* image) {
    mTextures.push_back(image);
}

void Background::Draw(SDL_Renderer* renderer) {
    int textWidth = 1280.0f;
    int textHeight = 768.0f;
    
    SDL_Rect r;
    r.w = static_cast<int>(textWidth * mOwner->GetScale());
    r.h = static_cast<int>(textHeight * mOwner->GetScale());
    // Center the rectangle around the position of the owner
    r.x = static_cast<int>(mOwner->GetPosition().x) - mParallax * mOwner->GetGame()->GetCameraPos().x;
    r.y = 0;

    bool filledScreen = false;
    int textIndex = 0;
    while (textIndex < mTextures.size() && !filledScreen) {
        SDL_Texture* texture = mTextures[textIndex];
        if (texture) {
            SDL_RenderCopyEx(renderer,
                         texture,
                         nullptr,
                         &r,
                         -Math::ToDegrees(mOwner->GetRotation()),
                         nullptr,
                         SDL_FLIP_NONE);
        
            r.x += textWidth;
        }
        if (r.x > 1280.0f) {
            filledScreen = true;
        }
        
        textIndex++;
        if (!filledScreen && textIndex == mTextures.size()) {
            textIndex = 0;
        }
    }
}
