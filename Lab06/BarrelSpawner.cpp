//
//  BarrelSpawner.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "BarrelSpawner.h"
#include "Barrel.h"
#include "SpriteComponent.h"
#include "Game.h"


BarrelSpawner::BarrelSpawner(class Game* game)
: Actor(game), timer(3.0f)
{
    Barrel* barrel = new Barrel(mGame);
    barrel->SetPosition(GetPosition());
    SpriteComponent* sc = new SpriteComponent(barrel);
    sc->SetTexture(mGame->GetTexture("Assets/Barrel.png"));
}
BarrelSpawner::~BarrelSpawner() { }

void BarrelSpawner::UpdateActor(float deltaTime) {
    timer -= deltaTime;
    if (timer <= 0.0f) {
        Barrel* barrel = new Barrel(mGame);
        barrel->SetPosition(GetPosition());
        SpriteComponent* sc = new SpriteComponent(barrel);
        sc->SetTexture(mGame->GetTexture("Assets/Barrel.png"));
        timer = 3.0f;
    }
}



