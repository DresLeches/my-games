//
//  Player.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Player.h"
#include "SpriteComponent.h"
#include <string>
#include "Game.h"
#include <algorithm>
#include "CollisionComponent.h"
#include "PlayerMove.h"

Player::Player(class Game* game)
: Actor(game)
{
    mSprite = new SpriteComponent(this);
    mCollision = new CollisionComponent(this);
    mMove = new PlayerMove(this);
    mCollision->SetSize(20.0f, 64.0f);
    AnimatedSprite* mAnimatedSprite = new AnimatedSprite(this);
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Player/Run1.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Player/Run2.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Player/Run3.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Player/Run4.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Player/Run5.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Player/Run6.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Player/Run7.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Player/Run8.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Player/Run9.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Player/Run10.png"));
    mSprite = mAnimatedSprite;
}

Player::~Player() {
    
}

void Player::SetInitialPos(Vector2 &pos) {
    initPos = pos;
}

Vector2 Player::GetInitialPos() {
    return initPos;
}
