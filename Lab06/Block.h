#pragma once
#include "Actor.h"
class Block : public Actor
{
public:
	Block(class Game* game);
	~Block();
	void SetBlockType(char type);
    void UpdateActor(float deltaTime);
private:
    // The offset when the position of the camera overtakes the position of the block
    // Supposed to help make the game look smoother
    int offSet;
};
