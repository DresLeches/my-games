#pragma once
#include "SDL/SDL.h"
#include "SDL/SDL_mixer.h"
#include <unordered_map>
#include <string>
#include <vector>
#include "Math.h"

class Game
{
public:
	Game();
	bool Initialize();
	void RunLoop();
	void Shutdown();

	void AddActor(class Actor* actor);
	void RemoveActor(class Actor* actor);

	void AddSprite(class SpriteComponent* sprite);
	void RemoveSprite(class SpriteComponent* sprite);

	void LoadTexture(const char* fileName);
	SDL_Texture* GetTexture(const char* fileName);
	void LoadLevel(const char* fileName);
    
    // Accessor function
    class Player* GetPlayer() const { return p; }
    const Vector2& GetCameraPos() const { return mCameraPos; }
    void SetCameraPos(Vector2& cameraPos) { mCameraPos = cameraPos; }
	
	// Vector of blocks
	std::vector<class Block*> mBlocks;
    void RemoveBlock(class Block* b);
    
    // Do some sounds
    void LoadSound(const std::string& filename);
    Mix_Chunk* GetSound(const std::string& filename);
    void PlayJumpSound();
    void PlayGotCoinSound();
    
    // Get the initial position
    int GetLevelPos() { return initLevel; }
    void LoadNextLevel();
    
    
private:
	void ProcessInput();
	void UpdateGame();
	void GenerateOutput();
	void LoadData();
	void UnloadData();
    
    
    // Actors
    class Player* p;

	// Map of textures loaded
	std::unordered_map<std::string, SDL_Texture*> mTextures;
    
    // Map of the sound
    std::unordered_map<std::string, Mix_Chunk*> mSound;

	// All the actors in the game
	std::vector<class Actor*> mActors;

	// All the sprite components drawn
	std::vector<class SpriteComponent*> mSprites;
    
    // Everything about the levels
    int nextLevel;
    std::vector<std::string> levels;
    
    // Keep track of where the next level should appear
    int initLevel;
    
    // Camera
    Vector2 mCameraPos;
    
	
	SDL_Window* mWindow;
	SDL_Renderer* mRenderer;
	Uint32 mTicksCount;
	bool mIsRunning;
};
