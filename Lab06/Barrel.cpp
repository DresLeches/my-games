//
//  Barrel.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "Barrel.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "BarrelMove.h"

Barrel::Barrel(class Game* game)
    : Actor(game)
{
    mSprite = new SpriteComponent(this);
    mCollision = new CollisionComponent(this);
    mMove = new BarrelMove(this);
    mCollision->SetSize(32.0f, 32.0f);
}
Barrel::~Barrel() {
    
}
