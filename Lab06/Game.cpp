//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
#include "SDL/SDL_image.h"
#include <algorithm>
#include "SpriteComponent.h"
#include "Actor.h"
#include "Block.h"
#include "Player.h"
#include "Background.h"
#include "BarrelSpawner.h"
#include "Coin.h"
#include <fstream>
#include <iostream>

Game::Game()
:mWindow(nullptr)
,mRenderer(nullptr)
,mIsRunning(true)
,mCameraPos(0.0f, 0.0f)
,nextLevel(0)
,initLevel(0)
{
	
}

bool Game::Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) != 0)
	{
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return false;
	}
	
	mWindow = SDL_CreateWindow("ITP Game", 100, 100, 1024, 768, 0);
	if (!mWindow)
	{
		SDL_Log("Failed to create window: %s", SDL_GetError());
		return false;
	}
	
	mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (!mRenderer)
	{
		SDL_Log("Failed to create renderer: %s", SDL_GetError());
		return false;
	}
	
	if (IMG_Init(IMG_INIT_PNG) == 0)
	{
		SDL_Log("Unable to initialize SDL_image: %s", SDL_GetError());
		return false;
	}
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
    
    LoadSound("Assets/Player/Jump.wav");
    LoadSound("Assets/Coin/coin.wav");

	LoadData();

	mTicksCount = SDL_GetTicks();
	
	return true;
}

void Game::RunLoop()
{
	while (mIsRunning)
	{
		ProcessInput();
		UpdateGame();
		GenerateOutput();
	}
}

void Game::ProcessInput()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_QUIT:
				mIsRunning = false;
				break;
		}
	}
	
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if (state[SDL_SCANCODE_ESCAPE])
	{
		mIsRunning = false;
	}
	
	for (auto actor : mActors)
	{
		actor->ProcessInput(state);
	}
}

void Game::UpdateGame()
{
    // Compute delta time
    Uint32  tickNow = SDL_GetTicks();
    // Wait until 16ms has elapsed since last frame
    while (tickNow - mTicksCount < 16)
    {
        tickNow = SDL_GetTicks();
    }

    // Get deltaTime in seconds        
    float deltaTime = (tickNow - mTicksCount) / 1000.0f;
    // Don't let deltaTime be greater than 0.05f (50 ms)
    if (deltaTime > 0.05f)
    {
        deltaTime = 0.05f;
    }
    mTicksCount = tickNow;

	// Make copy of actor vector
	// (iterate over this in case new actors are created)
	std::vector<Actor*> copy = mActors;
	// Update all actors
	for (auto actor : copy)
	{
		actor->Update(deltaTime);
	}

	// Add any dead actors to a temp vector
	std::vector<Actor*> deadActors;
	for (auto actor : mActors)
	{
		if (actor->GetState() == Actor::EDead)
		{
			deadActors.emplace_back(actor);
		}
	}

	// Delete any of the dead actors (which will
	// remove them from mActors)
	for (auto actor : deadActors)
	{
		delete actor;
	}
}

void Game::GenerateOutput()
{
	SDL_SetRenderDrawColor(mRenderer, 0, 0, 255, 255);
	SDL_RenderClear(mRenderer);
	
	// Draw all sprite components
	for (auto sprite : mSprites)
	{
		sprite->Draw(mRenderer);
	}
	SDL_RenderPresent(mRenderer);
}

void Game::LoadData()
{
    LoadTexture("Assets/Player/Idle.png");
	LoadTexture("Assets/BlockA.png");
	LoadTexture("Assets/BlockB.png");
	LoadTexture("Assets/BlockC.png");
	LoadTexture("Assets/BlockD.png");
	LoadTexture("Assets/BlockE.png");
	LoadTexture("Assets/BlockF.png");
    LoadTexture("Assets/Barrel.png");
    LoadTexture("Assets/Background/Sky_0.png");
    LoadTexture("Assets/Background/Sky_1.png");
    LoadTexture("Assets/Background/Sky_2.png");
    LoadTexture("Assets/Background/Mid_0.png");
    LoadTexture("Assets/Background/Mid_1.png");
    LoadTexture("Assets/Background/Mid_2.png");
    LoadTexture("Assets/Background/Fore_0.png");
    LoadTexture("Assets/Background/Fore_1.png");
    LoadTexture("Assets/Background/Fore_2.png");
    LoadTexture("Assets/Coin/coin1.png");
    LoadTexture("Assets/Coin/coin2.png");
    LoadTexture("Assets/Coin/coin3.png");
    LoadTexture("Assets/Coin/coin4.png");
    LoadTexture("Assets/Coin/coin5.png");
    LoadTexture("Assets/Coin/coin6.png");
    LoadTexture("Assets/Coin/coin7.png");
    LoadTexture("Assets/Coin/coin8.png");
    LoadTexture("Assets/Coin/coin9.png");
    LoadTexture("Assets/Coin/coin10.png");
    LoadTexture("Assets/Coin/coin11.png");
    LoadTexture("Assets/Coin/coin12.png");
    LoadTexture("Assets/Coin/coin13.png");
    LoadTexture("Assets/Coin/coin14.png");
    LoadTexture("Assets/Coin/coin15.png");
    LoadTexture("Assets/Coin/coin16.png");
    LoadTexture("Assets/Player/Run1.png");
    LoadTexture("Assets/Player/Run2.png");
    LoadTexture("Assets/Player/Run3.png");
    LoadTexture("Assets/Player/Run4.png");
    LoadTexture("Assets/Player/Run5.png");
    LoadTexture("Assets/Player/Run6.png");
    LoadTexture("Assets/Player/Run7.png");
    LoadTexture("Assets/Player/Run8.png");
    LoadTexture("Assets/Player/Run9.png");
    LoadTexture("Assets/Player/Run10.png");
    
    Actor* test = new Actor(this);
    test->SetPosition(Vector2(-512.0f, 0.0f));
    
    // Background 1
    Background* b1 = new Background(test, 1);
    b1->AddImage(GetTexture("Assets/Background/Sky_0.png"));
    b1->AddImage(GetTexture("Assets/Background/Sky_1.png"));
    b1->AddImage(GetTexture("Assets/Background/Sky_2.png"));
    b1->SetParallax(0.25f);
    
    // Background 2
    Background* b2 = new Background(test, 2);
    b2->AddImage(GetTexture("Assets/Background/Mid_0.png"));
    b2->AddImage(GetTexture("Assets/Background/Mid_1.png"));
    b2->AddImage(GetTexture("Assets/Background/Mid_2.png"));
    b2->SetParallax(0.5f);
    
    // Background 3
    Background* b3 = new Background(test, 3);
    b3->AddImage(GetTexture("Assets/Background/Fore_0.png"));
    b3->AddImage(GetTexture("Assets/Background/Fore_1.png"));
    b3->AddImage(GetTexture("Assets/Background/Fore_2.png"));
    b3->SetParallax(0.75f);
    
    
	//LoadLevel("Assets/Level0.txt");
    levels.push_back("Assets/Level0.txt");
    levels.push_back("Assets/Level1.txt");
    levels.push_back("Assets/Level2.txt");
    levels.push_back("Assets/Level3.txt");
    
    LoadNextLevel();
}

void Game::LoadNextLevel() {
    LoadLevel(levels[nextLevel].c_str());
    nextLevel++;
    if (nextLevel == 4) {
        nextLevel = 0;
    }
}

void Game::LoadLevel(const char* fileName)
{
	std::ifstream file(fileName);
	if (!file.is_open())
	{
		SDL_Log("Failed to load level: %s", fileName);
	}
	
	size_t row = 0;
	std::string line;
	while (!file.eof())
	{
		std::getline(file, line);
		for (size_t col = 0; col < line.size(); col++)
		{
			if (line[col] != '.')
			{
				Vector2 pos;
                pos.x = 32.0f + 64.0f * col + initLevel;
                pos.y = 16.0f + 32.0f * row;
                if (line[col] == 'P' && p == NULL) {
                    p = new Player(this);
                    p->SetPosition(pos);
                    p->SetInitialPos(pos);
                }
                else if (line[col] == '*') {
                    Coin* c = new Coin(this);
                    Vector2 tempPos(pos.x, pos.y-16.0f);
                    c->SetPosition(tempPos);
                }
                else if (line[col] == 'A' || line[col] == 'B' || line[col] == 'C' || line[col] == 'D') {
                    Block* b = new Block(this);
                    b->SetPosition(pos);
                    b->SetBlockType(line[col]);
                }
			}
		}
		row++;
	}
    initLevel += 56 * 64;
}

void Game::UnloadData()
{
	// Delete actors
	// Because ~Actor calls RemoveActor, have to use a different style loop
	while (!mActors.empty())
	{
		delete mActors.back();
	}

	// Destroy textures
	for (auto i : mTextures)
	{
		SDL_DestroyTexture(i.second);
	}
	mTextures.clear();
    
    // Destroy the sounds
    for (auto i : mSound) {
        Mix_FreeChunk(i.second);
    }
    mSound.clear();
}

void Game::LoadTexture(const char* fileName)
{
	// Load from file
	SDL_Surface* surf = IMG_Load(fileName);
	if (!surf)
	{
		SDL_Log("Failed to load texture file %s", fileName);
		return;
	}

	// Create texture from surface
	SDL_Texture* text = SDL_CreateTextureFromSurface(mRenderer, surf);
	SDL_FreeSurface(surf);
	if (!text)
	{
		SDL_Log("Failed to convert surface to texture for %s", fileName);
		return;
	}
	
	mTextures.emplace(fileName, text);
}

SDL_Texture* Game::GetTexture(const char * fileName)
{
	SDL_Texture* tex = nullptr;
	auto iter = mTextures.find(fileName);
	if (iter != mTextures.end())
	{
		tex = iter->second;
	}
	return tex;
}

void Game::Shutdown()
{
	UnloadData();
    Mix_CloseAudio();
	IMG_Quit();
	SDL_DestroyRenderer(mRenderer);
	SDL_DestroyWindow(mWindow);
	SDL_Quit();
}

void Game::AddActor(Actor* actor)
{
	mActors.emplace_back(actor);
}

void Game::RemoveActor(Actor* actor)
{
	auto iter = std::find(mActors.begin(), mActors.end(), actor);
	if (iter != mActors.end())
	{
		// Swap to end of vector and pop off (avoid erase copies)
		auto iter2 = mActors.end() - 1;
		std::iter_swap(iter, iter2);
		mActors.pop_back();
	}
}

void Game::AddSprite(SpriteComponent* sprite)
{
	mSprites.emplace_back(sprite);
	std::sort(mSprites.begin(), mSprites.end(), [](SpriteComponent* a, SpriteComponent* b) {
		return a->GetDrawOrder() < b->GetDrawOrder();
	});
}

void Game::RemoveSprite(SpriteComponent* sprite)
{
	auto iter = std::find(mSprites.begin(), mSprites.end(), sprite);
	mSprites.erase(iter);
}

void Game::RemoveBlock(Block *b) {
    auto iter = std::find(mBlocks.begin(),
                          mBlocks.end(),
                          b);
    mBlocks.erase(iter);
}

void Game::LoadSound(const std::string& filename) {
    mSound.emplace(filename, GetSound(filename));
}

Mix_Chunk* Game::GetSound(const std::string& filename) {
    return Mix_LoadWAV(filename.c_str());
}

void Game::PlayJumpSound() {
    Mix_PlayChannel(-1, mSound["Assets/Player/Jump.wav"], 0);
}

void Game::PlayGotCoinSound() {
    Mix_PlayChannel(-1, mSound["Assets/Coin/coin.wav"], 0);
}



