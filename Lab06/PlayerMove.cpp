//
//  PlayerMove.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "PlayerMove.h"
#include "Actor.h"
#include "Player.h"
#include "Block.h"
#include "Game.h"
#include "CollisionComponent.h"
#include <iostream>
#include <algorithm>

PlayerMove::PlayerMove(class Actor* owner)
:MoveComponent(owner),
mYSpeed(0.0f),
mSpacePressed(false),
mInAir(false),
mJumpTimer(0.0f),
timeLimit(0.3f),
playerOffset(512.0f)
{
    SetForwardSpeed(300.0f);
}

PlayerMove::~PlayerMove() { }

void PlayerMove::Update(float deltaTime) {
    Vector2 pos = mOwner->GetPosition();
    pos += mOwner->GetForward() * GetForwardSpeed() * deltaTime;
    
    if (mInAir && mSpacePressed && mJumpTimer < timeLimit) {
        mYSpeed -= 1500.0f * deltaTime;
        mJumpTimer += deltaTime;
    }
    
    pos.y += mYSpeed * deltaTime;
    
    if (pos.y > 768.0f) {
        pos.y = 768.0f;
        mInAir = false;
    }
    mOwner->SetPosition(pos);
    Vector2 cameraPos(pos.x - playerOffset, 0.0f);
    mOwner->GetGame()->SetCameraPos(cameraPos);
    mYSpeed += 2000.0f * deltaTime;
    
    std::vector<Block*> blocks = mOwner->GetGame()->mBlocks;
    for (auto block : blocks) {
        if (mOwner->GetCollision()->Intersect(block->GetCollision())) {
            Vector2 aMin = mOwner->GetCollision()->GetMin();
            Vector2 aMax = mOwner->GetCollision()->GetMax();
            Vector2 bMin = block->GetCollision()->GetMin();
            Vector2 bMax = block->GetCollision()->GetMax();
            
            float dx1 = Math::Abs(aMax.x - bMin.x);
            float dx2 = Math::Abs(aMin.x - bMax.x);
            float dy1 = Math::Abs(aMax.y - bMin.y);
            float dy2 = Math::Abs(aMin.y - bMax.y);
            float minDiff = std::min({dx1, dx2, dy1, dy2});
            
            if (dx1 == minDiff) {
                // Left collision has occurred
                pos.x -= dx1;
            }
            else if (dx2 == minDiff) {
                // Right collision has occurred
                pos.x += dx2;
            }
            else if (dy1 == minDiff) {
                // Top collision has occurred
                pos.y -= dy1;
                mYSpeed = 0.0f;
                mInAir = false;
            }
            else if (dy2 == minDiff) {
                // Bottom collision has occurred
                pos.y += dy2;
                if (mYSpeed < 0) {
                    mYSpeed = 0.0f;
                }
            }
            mOwner->SetPosition(pos);
            cameraPos.x = pos.x - playerOffset;
            mOwner->GetGame()->SetCameraPos(cameraPos);
        }
    }
    if ((int)(cameraPos.x / (mOwner->GetGame()->GetLevelPos() - 1024))) {
        mOwner->GetGame()->LoadNextLevel();
    }
 }
void PlayerMove::ProcessInput(const Uint8 *keyState) {
    if (!mSpacePressed && !mInAir && keyState[SDL_SCANCODE_SPACE]) {
        mYSpeed = -500.0f;
        mInAir = true;
        mOwner->GetGame()->PlayJumpSound();
        mJumpTimer = 0.0f;
    }
    mSpacePressed = keyState[SDL_SCANCODE_SPACE];
}
