//
//  Background.h
//  Game-mac
//
//  Created by Pablo Chung on 2/22/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Background_h
#define Background_h

#include "SpriteComponent.h"
#include "SDL/SDL.h"
#include <vector>

class Background : public SpriteComponent {
public:
    Background(class Actor* owner, int drawOrder);
    ~Background();
    void AddImage(SDL_Texture* image);
    void Draw(SDL_Renderer* sdlr);
    float GetParallax() { return mParallax; }
    void SetParallax(float parallax) { mParallax = parallax; }
private:
    std::vector<SDL_Texture*> mTextures;
    //int textIndex;
    int mTextWidth;
    int mTextHeight;
    float mParallax;
};

#endif /* Background_h */
