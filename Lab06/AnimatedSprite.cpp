//
//  AnimatedSprite.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/28/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "AnimatedSprite.h"
#include "Actor.h"
#include "Game.h"

AnimatedSprite::AnimatedSprite(class Actor* owner)
: SpriteComponent(owner), mAnimTimer(0.0f), mAnimSpeed(15.0f)
{
}

AnimatedSprite::~AnimatedSprite() {
    
}

void AnimatedSprite::AddImage(SDL_Texture* image) {
    mImages.push_back(image);
}

void AnimatedSprite::Update(float deltaTime) {
    mAnimTimer += mAnimSpeed * deltaTime;
    int frame = static_cast<int>(mAnimTimer) % mImages.size();
    SetTexture(mImages[frame]);
}
