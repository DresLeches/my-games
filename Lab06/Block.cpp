#include "Block.h"
#include "SpriteComponent.h"
#include <string>
#include "Game.h"
#include <algorithm>
#include "CollisionComponent.h"

Block::Block(class Game* game)
:Actor(game), offSet(100)
{
	mSprite = new SpriteComponent(this);
	mCollision = new CollisionComponent(this);
	mCollision->SetSize(64.0f, 32.0f);
	
	GetGame()->mBlocks.emplace_back(this);
}

Block::~Block()
{
    GetGame()->RemoveBlock(this);
}

void Block::SetBlockType(char type)
{
	std::string name = "Assets/Block";
	name += type;
	name += ".png";
	mSprite->SetTexture(GetGame()->GetTexture(name.c_str()));
}

void Block::UpdateActor(float deltaTime) {
    if (mGame->GetCameraPos().x > GetPosition().x + offSet) {
        SetState(Actor::EDead);
    }
}
