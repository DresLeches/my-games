//
//  Barrel.h
//  Game-mac
//
//  Created by Pablo Chung on 2/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//
#ifndef Barrel_h
#define Barrel_h
#include "Actor.h"

class Barrel : public Actor {
public:
    Barrel(class Game* game);
    ~Barrel();
};

#endif /* Barrel_h */
