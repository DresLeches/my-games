//
//  AnimatedSprite.h
//  Game-mac
//
//  Created by Pablo Chung on 2/28/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef AnimatedSprite_h
#define AnimatedSprite_h

#include "SpriteComponent.h"
#include <vector>
class AnimatedSprite : public SpriteComponent {
public:
    AnimatedSprite(class Actor* owner);
    ~AnimatedSprite();
    void AddImage(SDL_Texture* image);
    void Update(float deltaTime) override;
    void SetAnimationSpeed(float animSpeed) { mAnimSpeed = animSpeed; }
private:
    std::vector<SDL_Texture*> mImages;
    float mAnimTimer;
    float mAnimSpeed;
};

#endif /* AnimatedSprite_h */
