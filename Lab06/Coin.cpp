//
//  Coin.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/28/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "Coin.h"
#include "AnimatedSprite.h"
#include "CollisionComponent.h"
#include "MoveComponent.h"
#include "Game.h"
#include "Player.h"
/*
 */
Coin::Coin(class Game* game)
: Actor(game), offSet(100.0f)
{
    mCollision = new CollisionComponent(this);
    mMove = new MoveComponent(this);
    mCollision->SetSize(32.0f, 32.0f);
    AnimatedSprite* mAnimatedSprite = new AnimatedSprite(this);
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin1.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin2.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin3.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin4.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin5.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin6.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin7.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin8.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin9.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin10.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin11.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin12.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin13.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin14.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin15.png"));
    mAnimatedSprite->AddImage(GetGame()->GetTexture("Assets/Coin/coin16.png"));
    mSprite = mAnimatedSprite;
}
Coin::~Coin() {

}

void Coin::UpdateActor(float deltaTime) {
    if (GetCollision()->Intersect(mGame->GetPlayer()->GetCollision())) {
        mGame->PlayGotCoinSound();
        SetState(Actor::EDead);
    }
    else if (mGame->GetPlayer()->GetGame()->GetCameraPos().x > GetPosition().x + offSet) {
        SetState(Actor::EDead);
    }
}
