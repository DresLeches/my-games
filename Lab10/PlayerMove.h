#pragma once
#include "MoveComponent.h"
#include "Math.h"

class PlayerMove : public MoveComponent
{
public:
	enum MoveState
	{
		OnGround,
		Jump,
		Falling,
        WallClimb,
        WallRun
	};

	enum CollSide
	{
		None,
		Top,
		Bottom,
		SideX1,
        SideX2,
        SideY1,
        SideY2
	};
	PlayerMove(class Actor* owner);
	void Update(float deltaTime) override;
	void ProcessInput(const Uint8* keyState) override;
	void ChangeState(MoveState state);
    void PhysicsUpdate(float deltaTime);
    void AddForce(const Vector3& force);
    void FixXYVelocity();
    bool oppositeSign(float val1, float val2);
    bool CanWallClimb(CollSide cs);
    bool CanWallRun(CollSide cs);
protected:
	void UpdateOnGround(float deltaTime);
	void UpdateJump(float deltaTime);
	void UpdateFalling(float deltaTime);
    void UpdateWallClimb(float deltaTime);
    void UpdateWallRun(float deltaTime);
	CollSide FixCollision(class CollisionComponent* self, class CollisionComponent* block);
private:
    // Current state of the player
	MoveState mCurrentState = Falling;
    
    // Physics property of player
	float mZSpeed = 0.0f;
	const float Gravity = -980.0f;
	const float JumpSpeed = 500.0f;
    
    // Player information
	bool mSpacePressed = false;
    Vector3 mVelocity;
    Vector3 mAccleration;
    Vector3 mPendingForces;
    Vector3 mGravity;
    Vector3 mJumpForce;
    Vector3 wallClimbForce;
    Vector3 mWallRunForce;
    
    float mMass;
    float mWallClimbTimer;
    float mWallRunTimer;
};
