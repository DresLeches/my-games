#include "PlayerMove.h"
#include "Actor.h"
#include "Game.h"
#include "CollisionComponent.h"
#include "Block.h"
#include <SDL/SDL.h>
#include "CameraComponent.h"
#include <iostream>

PlayerMove::PlayerMove(class Actor* owner)
:MoveComponent(owner)
,mGravity(0.0f, 0.0f, -980.0f)
,mJumpForce(0.0f, 0.0f, 35000.0f)
,mMass(1.0f)
,wallClimbForce(0.0f, 0.0f, 1800.0f)
,mWallClimbTimer(0.0f)
,mWallRunForce(0.0f, 0.0f, 1200.0f)
{
	ChangeState(Falling);
}

void PlayerMove::Update(float deltaTime)
{
	switch (mCurrentState)
	{
	case OnGround:
		UpdateOnGround(deltaTime);
		break;
	case Jump:
		UpdateJump(deltaTime);
		break;
	case Falling:
		UpdateFalling(deltaTime);
		break;
    case WallClimb:
        UpdateWallClimb(deltaTime);
        break;
    case WallRun:
        UpdateWallRun(deltaTime);
        break;
	default:
		break;
	}
}
bool stop = false;
void PlayerMove::ProcessInput(const Uint8* keyState)
{
    if (keyState[SDL_SCANCODE_0]) {
        stop = true;
    }
	// Update forward speed
	if (keyState[SDL_SCANCODE_W])
	{
        AddForce(700.0f * mOwner->GetForward());
	}
	if (keyState[SDL_SCANCODE_S])
	{
        AddForce(-700.0f * mOwner->GetForward());
	}

	// Update strafe speed
	if (keyState[SDL_SCANCODE_D])
	{
        AddForce(700.0f * mOwner->GetRight());
	}
	if (keyState[SDL_SCANCODE_A])
	{
        AddForce(-700.0f * mOwner->GetRight());
	}

	int x, y;
	SDL_GetRelativeMouseState(&x, &y);
	float angularSpd = (x / 500.0f) * Math::Pi * 10.0f;
	SetAngularSpeed(angularSpd);
	float pitchSpd = (y / 500.0f) * Math::Pi * 10.0f;
	mOwner->GetCamera()->SetPitchSpeed(pitchSpd);

	// Jumping
	if (!mSpacePressed && keyState[SDL_SCANCODE_SPACE] && mCurrentState == OnGround)
	{
        AddForce(mJumpForce);
        ChangeState(Jump);
	}

	mSpacePressed = keyState[SDL_SCANCODE_SPACE];
}

void PlayerMove::ChangeState(MoveState state)
{
	mCurrentState = state;
}

void PlayerMove::UpdateOnGround(float deltaTime)
{
    PhysicsUpdate(deltaTime);

	bool onBlock = false;
	for (Block* block : mOwner->GetGame()->mBlocks)
	{
		CollSide side = FixCollision(mOwner->GetCollision(), block->GetCollision());
		if (side == Top)
		{
			onBlock = true;
		}
        else if (side == PlayerMove::SideX1 || side == PlayerMove::SideX2 ||
                 side == PlayerMove::SideY1 || side == PlayerMove::SideY2)
        {
            if (CanWallClimb(side)) {
                ChangeState(WallClimb);
                mWallClimbTimer = 0.0f;
                return;
            }
        }
	}

	if (!onBlock)
	{
		ChangeState(Falling);
	}
}

void PlayerMove::UpdateJump(float deltaTime)
{
    AddForce(mGravity);
    PhysicsUpdate(deltaTime);
	for (Block* block : mOwner->GetGame()->mBlocks)
	{
		CollSide side = FixCollision(mOwner->GetCollision(), block->GetCollision());
		if (side == Bottom)
		{
            mVelocity.z = 0.0f;
		}
        else if (side == PlayerMove::SideX1 || side == PlayerMove::SideX2 ||
                 side == PlayerMove::SideY1 || side == PlayerMove::SideY2)
        {
            if (CanWallClimb(side)) {
                ChangeState(WallClimb);
                mWallClimbTimer = 0.0f;
                return;
            }
            else if (CanWallRun(side)) {
                ChangeState(WallRun);
                mWallRunTimer = 0.0f;
            }
        }
        
	}

	if (mVelocity.z <= 0.0f)
	{
        
		ChangeState(Falling);
	}
}

void PlayerMove::UpdateFalling(float deltaTime)
{
    AddForce(mGravity);
    PhysicsUpdate(deltaTime);
    for (Block* block : mOwner->GetGame()->mBlocks)
    {
        CollSide side = FixCollision(mOwner->GetCollision(), block->GetCollision());
        if (side == Top)
        {
            mVelocity.z = 0.0f;
            ChangeState(OnGround);
        }
    }
}

PlayerMove::CollSide PlayerMove::FixCollision(CollisionComponent* self, CollisionComponent* block)
{
    PlayerMove::CollSide tempState = PlayerMove::None;
    Vector3 pos = mOwner->GetPosition();
    
    if (self->Intersect(block)) {
        Vector3 aMin = self->GetMin();
        Vector3 aMax = self->GetMax();
        Vector3 bMin = block->GetMin();
        Vector3 bMax = block->GetMax();
        
        float dx1 = Math::Abs(aMax.x - bMin.x);
        float dx2 = Math::Abs(aMin.x - bMax.x);
        float dy1 = Math::Abs(aMax.y - bMin.y);
        float dy2 = Math::Abs(aMin.y - bMax.y);
        float dz1 = Math::Abs(aMax.z - bMin.z);
        float dz2 = Math::Abs(aMin.z - bMax.z);
        float minDiff = std::min({dx1, dx2, dy1, dy2, dz1, dz2});
        
        if (dx1 == minDiff) {
            // Left collision has occurred
            pos.x -= dx1;
            tempState = PlayerMove::SideX1;
            Vector3 normie(-700.0f, 0.0f, 0.0f);
            AddForce(normie);
        }
        else if (dx2 == minDiff) {
            // Right collision has occurred
            pos.x += dx2;
            tempState = PlayerMove::SideX2;
            Vector3 normie(700.0f, 0.0f, 0.0f);
            AddForce(normie);
        }
        else if (dy1 == minDiff) {
            // Top collision has occurred
            pos.y -= dy1;
            tempState = PlayerMove::SideY1;
            Vector3 normie(0.0f, -700.0f, 0.0f);
            AddForce(normie);
        }
        else if (dy2 == minDiff) {
            // Bottom collision has occurred
            pos.y += dy2;
            tempState = PlayerMove::SideY2;
            Vector3 normie(0.0f, 700.0f, 0.0f);
            AddForce(normie);
        }
        else if (dz1 == minDiff) {
            // Top z collision has occurred
            pos.z -= dz1;
            tempState = PlayerMove::Bottom;
        }
        else if (dz2 == minDiff) {
            // Bottom z collision has occurred
            pos.z += dz2;
            tempState = PlayerMove::Top;
        }
        mOwner->SetPosition(pos);
    }
    return tempState;
}

void PlayerMove::PhysicsUpdate(float deltaTime) {
    
    // Clamp it down :)
    if (deltaTime > 0.016) {
        deltaTime = 0.016;
    }
    mAccleration = mPendingForces * (1.0f / mMass);
    
    mVelocity += mAccleration * deltaTime;
    FixXYVelocity();
    Vector3 pos = mOwner->GetPosition();
    pos += mVelocity * deltaTime;
    mOwner->SetPosition(pos);
    
    mOwner->SetRotation(mOwner->GetRotation() + GetAngularSpeed() * deltaTime);
    mPendingForces = Vector3::Zero;
}
void PlayerMove::AddForce(const Vector3& force) {
    mPendingForces += force;
}

void PlayerMove::FixXYVelocity() {
    Vector2 xyVelocity(mVelocity.x, mVelocity.y);
    float magnitude = sqrt(Vector2::Dot(xyVelocity, xyVelocity));
    
    if (magnitude > 400.0f) {
        xyVelocity = Vector2::Normalize(xyVelocity) * 400.0f;
    }
    if (mCurrentState == PlayerMove::OnGround || mCurrentState == PlayerMove::WallClimb) {
        if (Math::NearZero(mAccleration.x)) {
            xyVelocity.x *= 0.9f;
        }
        if (Math::NearZero(mAccleration.y)) {
            xyVelocity.y *= 0.9f;
        }
        
        // If multiplying the acceleration and velocity gives negative
        //   then they have to be opposite signs
        if (mAccleration.x * xyVelocity.x < 0) {
            xyVelocity.x *= 0.9f;
        }
        if (mAccleration.y * xyVelocity.y < 0) {
            xyVelocity.y *= 0.9f;
        }
    }
    mVelocity.x = xyVelocity.x;
    mVelocity.y = xyVelocity.y;
}

bool PlayerMove::oppositeSign(float val1, float val2) {
    return (val1 < 0 && val2 > 0) || (val1 > 0 && val2 < 0);
}

void PlayerMove::UpdateWallClimb(float deltaTime) {
    bool noCollision = false;
    
    mWallClimbTimer += deltaTime;
    AddForce(mGravity);
    if (mWallClimbTimer < 0.4f) {
        AddForce(wallClimbForce);
    }
    PhysicsUpdate(deltaTime);
    for (Block* block : mOwner->GetGame()->mBlocks)
    {
        CollSide side = FixCollision(mOwner->GetCollision(), block->GetCollision());
        
        if (side == PlayerMove::SideX1 ||
            side == PlayerMove::SideX2 ||
            side == PlayerMove::SideY1 ||
            side == PlayerMove::SideY2)
        {
            noCollision = true;
            break;
        }
    }
    if (!noCollision || mVelocity.z <= 0.0f) {
        mVelocity.z = 0.0f;
        ChangeState(PlayerMove::Falling);
    }
}

void PlayerMove::UpdateWallRun(float deltaTime) {
    
    mWallRunTimer += deltaTime;
    AddForce(mGravity);
    if (mWallRunTimer < 0.4f) {
        AddForce(mWallRunForce);
    }
    PhysicsUpdate(deltaTime);
    for (Block* block : mOwner->GetGame()->mBlocks)
    {
        CollSide side = FixCollision(mOwner->GetCollision(), block->GetCollision());
        
        if (side == PlayerMove::SideX1 ||
            side == PlayerMove::SideX2 ||
            side == PlayerMove::SideY1 ||
            side == PlayerMove::SideY2)
        {
            break;
        }
    }
    if (mVelocity.z <= 0.0f) {
        mVelocity.z = 0.0f;
        ChangeState(PlayerMove::Falling);
    }
}

bool PlayerMove::CanWallClimb(CollSide cs) {
    if (stop) {
        std::cout << "Hi" << std::endl;
        stop = false;
    }
    Vector3 res;
    switch (cs) {
    case SideX1:
        res.Set(-1.0, 0.0, 0.0);
        break;
    case SideX2:
        res.Set(1.0, 0.0, 0.0);
        break;
    case SideY1:
        res.Set(0.0, -1.0, 0.0);
        break;
    case SideY2:
        res.Set(0.0, 1.0, 0.0);
        break;
    default:
        break;
    }
    
    // Is the player facing the wall
    float dotRes1 = Vector3::Dot(res, mOwner->GetForward());
    float magnitude1 = res.Length() * mOwner->GetForward().Length();
    float angle1 = Math::Acos(dotRes1/ magnitude1);
    angle1 = Math::ToDegrees(angle1);
    
    // Does the player come at the wall at a certain angle
    Vector2 res2D(res.x, res.y);
    Vector2 xyVelocity(mVelocity.x, mVelocity.y);
    float dotRes2 = Vector2::Dot(res2D, xyVelocity);
    float magnitude2 = res2D.Length() * xyVelocity.Length();
    float angle2 = Math::Acos(dotRes2/magnitude2);
    angle2 = Math::ToDegrees(angle2);
    
    // Does the player come at the wall at a high enough velocity
    float magnitude3 = xyVelocity.Length();
    
    return ((angle1 > 170.0f || angle1 < -170.0f) &&
            (angle2 > 170.0f || angle2 < -170.0f) &&
            (magnitude3 > 350.0f));
}
bool PlayerMove::CanWallRun(PlayerMove::CollSide cs) {
    if (stop) {
        std::cout << "Hi" << std::endl;
        stop = false;
    }
    Vector3 res;
    switch (cs) {
        case SideX1:
            res.Set(-1.0, 0.0, 0.0);
            break;
        case SideX2:
            res.Set(1.0, 0.0, 0.0);
            break;
        case SideY1:
            res.Set(0.0, -1.0, 0.0);
            break;
        case SideY2:
            res.Set(0.0, 1.0, 0.0);
            break;
        default:
            break;
    }
    
    // Is the player facing the wall
    float dotRes1 = Vector3::Dot(res, mOwner->GetForward());
    float magnitude1 = res.Length() * mOwner->GetForward().Length();
    float angle1 = Math::Acos(dotRes1/ magnitude1);
    angle1 = Math::ToDegrees(angle1);
    
    // Does the player come at the wall at a certain angle
    Vector2 res2D(res.x, res.y);
    Vector2 xyVelocity(mVelocity.x, mVelocity.y);
    float dotRes2 = Vector2::Dot(res2D, xyVelocity);
    float magnitude2 = res2D.Length() * xyVelocity.Length();
    float angle2 = Math::Acos(dotRes2/magnitude2);
    angle2 = Math::ToDegrees(angle2);
    
    // Does the player come at the wall at a high enough velocity
    float magnitude3 = xyVelocity.Length();
    
    return ((angle1 > 100.0f || angle1 < -100.0f) &&
            (angle2 > 100.0f || angle2 < -100.0f) &&
            (magnitude3 > 350.0f));
}
