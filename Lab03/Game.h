#pragma once

// Header files
#include "SDL/SDL.h"
#include "Actor.h"
#include <cmath>
#include <vector>
#include <unordered_map>
#include <string>

class Game {
public:
    Game();
    bool Initialize();
    void ShutDown();
    void RunLoop();
    
    // ProcessInput, UpdateGame, GenerateOuput)
    void ProcessInput();
    void UpdateGame();
    void GenerateOutput();
    
    // Manipulating the actors
    void AddActor(Actor* actor);
    void RemoveActor(Actor* actor);
    
    // Manipulating the sprites
    void AddSprite(SpriteComponent* sc);
    void RemoveSprite(SpriteComponent* sc);
    
    // Creating the texture
    //LoadTexture takes in the name of an image file, runs the image loading code, and adds the texture to your map
    //GetTexture takes in the name of an image file, and returns the texture from the map, if it exists.
    void LoadTexture(std::string imageName);
    SDL_Texture* GetTexture(std::string imageName);
    
    
private:
    // Window dim
    int w_;
    int h_;
    
    // Window object
    SDL_Window* sdlw;
    SDL_Renderer* sdlr;
    
    // SDL Texture
    SDL_Texture *bgt;
    std::unordered_map<std::string, SDL_Texture*> sprites;
     
    // Player quit
    bool playerQuit;
    bool playerLoses;

    // Keep track of the ticks
    Uint32 mTicksCount;
    
    // Actors
    std::vector<Actor*> actors;
    
    // Game Sprite
    std::vector<SpriteComponent*> mSprites;
    
    // Loading the data
    void LoadData();
    void UnloadData();
    
};

