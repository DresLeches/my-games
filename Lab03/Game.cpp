//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
#include <stdio.h>
#include <iostream>
#include <SDL/SDL_image.h>
#include <algorithm>
#include "Ship.h"

// TODO
Game::Game() { }

// Initialize the game
bool Game::Initialize() {
    
    // Initialize SDL
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return false;
    }
    
    // Initialize the game's member variables
    playerQuit = false;
    playerLoses = false;

    // Initialize the windows dimensions
    w_ = 1024;
    h_ = 768;
    
    // Creating the SDL Window
    sdlw = SDL_CreateWindow("Asteroids (Not Really)",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED,
                            w_,
                            h_,
                            0);
    
    // Create the SDL Renderer
    sdlr = SDL_CreateRenderer(sdlw, 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    
    // Load the image
    int flags = IMG_INIT_PNG;
    int imgInit = IMG_Init(flags);
    if((imgInit & flags) != flags) {
        printf("IMG_Init: Failed to init required png support!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
        return false;
    }
    
    // Creates the texture
    SDL_Surface *tempTexture;
    tempTexture = IMG_Load("Assets/Stars.png");
    if(!tempTexture) {
        printf("IMG_Load: %s\n", IMG_GetError());
        return false;
    }
    bgt = SDL_CreateTextureFromSurface(sdlr, tempTexture);
    SDL_FreeSurface(tempTexture);
    
    // Loading the actor data
    LoadData();
    
    // Get the number of ticks
    mTicksCount = SDL_GetTicks();
    
    // Successfully created the game
    return true;
}

// ShutDown the game
void Game::ShutDown() {
    SDL_DestroyRenderer(sdlr);
    SDL_DestroyWindow(sdlw);
    SDL_Quit();
    UnloadData();
    IMG_Quit();
}

// Run the loop
void Game::RunLoop() {
    while (!playerQuit && !playerLoses) {
        ProcessInput();
        UpdateGame();
        GenerateOutput();
    }
}

// Process the input
void Game::ProcessInput() {
    SDL_Event sdlEvent;
    bool quit = false;
    
    while (SDL_PollEvent(&sdlEvent)) {
        switch(sdlEvent.type) {
            case SDL_QUIT:
            {
                quit = true;
                break;
            }
        }
    }
    // Get the keyboard state
    const Uint8 *state = SDL_GetKeyboardState(NULL);
    
    // Process Input of Actors
    for (auto actor : actors) {
        actor->ProcessInput(state);
    }
    
    // Check if player wants to quit the game
    playerQuit = (state[SDL_SCANCODE_ESCAPE] || quit) ? true : false;
}

// Update the game
void Game::UpdateGame() {

    // Compute delta time
    Uint32  tickNow = SDL_GetTicks();
    // Wait until 16ms has elapsed since last frame
    while (tickNow - mTicksCount < 16) {
        tickNow = SDL_GetTicks();
    }

    // Get deltaTime in seconds        
    float deltaTime = (tickNow - mTicksCount) / 1000.0f;
    // Don't let deltaTime be greater than 0.05f (50 ms)
    if (deltaTime > 0.05f) {
        deltaTime = 0.05f;
    }
    mTicksCount = tickNow;
    
    // Update the actor time
    std::vector<Actor*> tempActors = actors;
    for (auto actor : tempActors) {
        actor->Update(deltaTime);
    }

    // Add any dead actors
    std::vector<Actor*> deadActors;
    for (auto actor : tempActors) {
        if (actor->GetState() == Actor::EDead) {
            deadActors.emplace_back(actor);
        }
    }
    for (auto deadActor : deadActors) {
        delete deadActor;
    }
}

// Generate the output
void Game::GenerateOutput() {
    if (SDL_SetRenderDrawColor(sdlr, 0, 0, 0, 255) != 0) {
        SDL_Log("Unable to generate output SDL: %s", SDL_GetError());
        return;
    }
    if (SDL_RenderClear(sdlr)) {
        SDL_Log("Unable to clear renderer SDL: %s", SDL_GetError());
        return;
    }
    
    // Mainly used for debugging
    SDL_RenderCopy(sdlr, bgt, nullptr, nullptr);
    
    for (auto it = mSprites.begin(); it != mSprites.end(); ++it) {
        (*it)->Draw(sdlr);
    }
    
    // Render the image
    SDL_RenderPresent(sdlr);
}

// Add actor
void Game::AddActor(Actor* actor) {
    actors.push_back(actor);
}

// Remove actor
void Game::RemoveActor(Actor* actor) {
	auto iter = std::find(actors.begin(), actors.end(), actor);
	if (iter != actors.end()) {
        // Swap to end of vector and pop off (avoid erase copies)
        auto iter2 = actors.end() - 1;
        std::iter_swap(iter, iter2);
        actors.pop_back();
	}
}

// Add the sprites
void Game::AddSprite(SpriteComponent *sc) {
    mSprites.push_back(sc);
    std::sort(mSprites.begin(), mSprites.end(),
              [](SpriteComponent* a, SpriteComponent* b) {
                  return a->GetDrawOrder() < b->GetDrawOrder();
              });
}

// Remove the sprites
void Game::RemoveSprite(SpriteComponent *sc) {
    mSprites.erase(std::find(mSprites.begin(), mSprites.end(), sc));
}

// Load the data
void Game::LoadData() {
    
    // Loading the textures
    LoadTexture("Assets/Asteroid.png");
    LoadTexture("Assets/Laser.png");
    LoadTexture("Assets/Ship.png");
    LoadTexture("Assets/ShipThrust.png");
    LoadTexture("Assets/Stars.png");
    
    // Creating the star texture
    Actor* test4 = new Actor(this);
    Vector2 pos3(512, 384);
    test4->SetPosition(pos3);
    SpriteComponent* sc4 = new SpriteComponent(test4);
    sc4->SetTexture(GetTexture("Assets/Stars.png"));
    test4->SetSprite(sc4);
    
    // Creating the ship
    Ship* ship = new Ship(this);
    Vector2 pos4(350, 350);
    ship->SetPosition(pos4);
}

// Unload the data
void Game::UnloadData() {
    
    // Emptying out the actors and the sprites
    while (!actors.empty()) {
        delete actors.back();
    }
	// Destroy textures
	for (auto i : sprites) {
		SDL_DestroyTexture(i.second);
	}
    sprites.clear();
}

// Get the texture
SDL_Texture* Game::GetTexture(std::string imageName) {
    auto it = sprites.find(imageName);
    if (it != sprites.end()) {
        return it->second;
    }
    return nullptr;
}

// Load the texture
void Game::LoadTexture(std::string imageName) {
    SDL_Texture* insText = nullptr;
    
    // Creates the texture
    SDL_Surface *tempTexture;
    tempTexture = IMG_Load(imageName.c_str());
    if(!tempTexture) {
        printf("IMG_Load: %s\n", IMG_GetError());
        return;
    }
    insText = SDL_CreateTextureFromSurface(sdlr, tempTexture);
    SDL_FreeSurface(tempTexture);
    sprites.insert(std::pair<std::string, SDL_Texture*>(imageName, insText));
}




