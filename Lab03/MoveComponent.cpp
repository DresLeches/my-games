#include "MoveComponent.h"
#include "Actor.h"
#include "Math.h"

MoveComponent::MoveComponent(class Actor* owner)
:Component(owner)
,mAngularSpeed(0.0f)
,mForwardSpeed(0.0f)
{
	
}

// Update the move component
void MoveComponent::Update(float deltaTime)
{
	// Set Rotation
    mOwner->SetRotation(deltaTime * mAngularSpeed + mOwner->GetRotation());
    
    // Set the forward position
    Vector2 unitVec = mOwner->GetFoward();
    unitVec = unitVec * (mForwardSpeed * deltaTime);
    Vector2 newPos = mOwner->GetPosition() + unitVec;
    
    mOwner->SetPosition(newPos);
}
