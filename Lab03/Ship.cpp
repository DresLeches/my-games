#include "Ship.h"
#include "Game.h"

// Ship constructor
Ship::Ship(class Game* game)
:  Actor(game)
{
    mSprite = new SpriteComponent(this);
    mMove = new MoveComponent(this);
    
    mSprite->SetTexture(mGame->GetTexture("Assets/Ship.png"));
}

// Ship actor input
void Ship::ActorInput(const Uint8 *keyState) {
    // Iterating through all the actors
    bool isMoving = false;
    const Uint8 *state = SDL_GetKeyboardState(NULL);
    if (state[SDL_SCANCODE_UP]) {
        mMove->SetForwardSpeed(200);
        isMoving = true;
    }
    else if (state[SDL_SCANCODE_DOWN]) {
        mMove->SetForwardSpeed(-200);
        isMoving = true;
    }
    else {
        mMove->SetForwardSpeed(0);
    }

    if (state[SDL_SCANCODE_LEFT]) {
        mMove->SetAngularSpeed(5);
    }
    else if (state[SDL_SCANCODE_RIGHT]) {
        mMove->SetAngularSpeed(-5);
    }
    else {
        mMove->SetAngularSpeed(0);
    }
    
    
    if (isMoving) {
        mSprite->SetTexture(mGame->GetTexture("Assets/ShipThrust.png"));
    }
    else {
        mSprite->SetTexture(mGame->GetTexture("Assets/Ship.png"));
    }
}




