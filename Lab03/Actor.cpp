#include "Actor.h"
#include "Component.h"
#include <algorithm>
#include <iostream>

// Create the constructors
Actor::Actor(Game* game)
	:mGame(game)
	,mState(EActive)
	,mPosition(Vector2::Zero)
	,mScale(1.0f)
	,mRotation(0.0f)
    ,mSprite(nullptr)
    ,mMove(nullptr)
{
	// TODO
    mGame->AddActor(this);
    mState = EActive;
}

// Kill off the actors but not really
Actor::~Actor()
{
	// TODO
    mGame->RemoveActor(this);
    mState = EDead;
    delete mSprite;
    delete mMove;
}

// Update the actor
void Actor::Update(float deltaTime)
{
    if (mState == EActive) {
        if (mMove != nullptr) {
            mMove->Update(deltaTime);
        }
        if (mSprite != nullptr) {
            mSprite->Update(deltaTime);
        }
        UpdateActor(deltaTime);
    }
}

// Update the actor
void Actor::UpdateActor(float deltaTime)
{
}

// Process the actor input
void Actor::ProcessInput(const Uint8* keyState)
{
	// TODO
    if (mState == EActive) {
        mSprite->ProcessInput(keyState);
        ActorInput(keyState);
    }
}

// Actor input
void Actor::ActorInput(const Uint8* keyState)
{
}

// Actor get forward
Vector2 Actor::GetFoward() {
    Vector2 unitVec(Math::Cos(GetRotation()), -Math::Sin(GetRotation()));
    return unitVec;
}
