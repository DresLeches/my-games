#include "Actor.h"

class Ship : public Actor {
public:
    Ship(class Game* game);
    void ActorInput(const Uint8* keyState);
};