//
//  Player.h
//  Game-mac
//
//  Created by Pablo Chung on 2/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//
#ifndef Player_h
#define Player_h
#include "Actor.h"
class Player : public Actor {
public:
    Player(class Game* game);
    ~Player();
    void SetInitialPos(Vector2& pos);
    Vector2 GetInitialPos();
private:
    Vector2 initPos;
};

#endif /* Player_h */
