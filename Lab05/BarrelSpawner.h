//
//  BarrelSpawner.h
//  Game-mac
//
//  Created by Pablo Chung on 2/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef BarrelSpawner_h
#define BarrelSpawner_h
#include "Actor.h"

class BarrelSpawner : public Actor {
public:
    BarrelSpawner(class Game* game);
    ~BarrelSpawner();
    void UpdateActor(float deltaTime);
private:
    float timer;
};

#endif /* BarrelSpawner_h */
