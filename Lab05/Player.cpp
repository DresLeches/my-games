//
//  Player.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//
/*
 #include "Block.h"
 #include "SpriteComponent.h"
 #include <string>
 #include "Game.h"
 #include <algorithm>
 #include "CollisionComponent.h"
 
 Block::Block(class Game* game)
 :Actor(game)
 {
 mSprite = new SpriteComponent(this);
 mCollision = new CollisionComponent(this);
 mCollision->SetSize(64.0f, 32.0f);
 
 GetGame()->mBlocks.emplace_back(this);
 }
 
 Block::~Block()
 {
 auto iter = std::find(GetGame()->mBlocks.begin(),
 GetGame()->mBlocks.end(),
 this);
 GetGame()->mBlocks.erase(iter);
 }
 
 void Block::SetBlockType(char type)
 {
 std::string name = "Assets/Block";
 name += type;
 name += ".png";
 mSprite->SetTexture(GetGame()->GetTexture(name.c_str()));
 }
 */

#include "Player.h"
#include "SpriteComponent.h"
#include <string>
#include "Game.h"
#include <algorithm>
#include "CollisionComponent.h"
#include "PlayerMove.h"

Player::Player(class Game* game)
: Actor(game)
{
    mSprite = new SpriteComponent(this);
    mCollision = new CollisionComponent(this);
    mMove = new PlayerMove(this);
    mCollision->SetSize(20.0f, 64.0f);
    
}

Player::~Player() {
    
}

void Player::SetInitialPos(Vector2 &pos) {
    initPos = pos;
}

Vector2 Player::GetInitialPos() {
    return initPos;
}
