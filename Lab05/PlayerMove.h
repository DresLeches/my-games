//
//  PlayerMove.h
//  Game-mac
//
//  Created by Pablo Chung on 2/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//
#ifndef PlayerMove_h
#define PlayerMove_h
#include "MoveComponent.h"
#include "Math.h"

class PlayerMove : public MoveComponent {
public:
    PlayerMove(class Actor* owner);
    ~PlayerMove();
    void Update(float deltaTime) override;
    void ProcessInput(const Uint8* keyState) override;
private:
    Vector2 mVelocity;
    float mYSpeed;
    bool mSpacePressed;
    bool mInAir;
};

#endif /* PlayerMove_h */
