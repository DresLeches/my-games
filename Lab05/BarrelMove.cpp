//
//  BarrelMove.cpp
//  Game-mac
//
//  Created by Pablo Chung on 2/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include <stdio.h>
#include "BarrelMove.h"
#include "Barrel.h"
#include "Game.h"
#include "CollisionComponent.h"
#include "Block.h"
#include "Player.h"

BarrelMove::BarrelMove(class Actor* actor)
: MoveComponent(actor), mYSpeed(0.0f)
{
    SetForwardSpeed(100.0f);
    SetAngularSpeed(-Math::TwoPi);
}
BarrelMove::~BarrelMove() {
    
}
void BarrelMove::Update(float deltaTime) {
    if (GetAngularSpeed()) {
        float rot = mOwner->GetRotation();
        rot += GetAngularSpeed() * deltaTime;
        mOwner->SetRotation(rot);
    }
    if (GetForwardSpeed()) {
        Vector2 pos = mOwner->GetPosition();
        pos.x += GetForwardSpeed() * deltaTime;
        pos.y += mYSpeed * deltaTime;
        mYSpeed += 2000.0f * deltaTime;
        mOwner->SetPosition(pos);
        
        std::vector<Block*> blocks = mOwner->GetGame()->mBlocks;
        for (auto block : blocks) {
            if (mOwner->GetCollision()->Intersect(block->GetCollision())) {
                Vector2 aMin = mOwner->GetCollision()->GetMin();
                Vector2 aMax = mOwner->GetCollision()->GetMax();
                Vector2 bMin = block->GetCollision()->GetMin();
                Vector2 bMax = block->GetCollision()->GetMax();
                
                float dx1 = Math::Abs(aMax.x - bMin.x);
                float dx2 = Math::Abs(aMin.x - bMax.x);
                float dy1 = Math::Abs(aMax.y - bMin.y);
                float dy2 = Math::Abs(aMin.y - bMax.y);
                float minDiff = std::min({dx1, dx2, dy1, dy2});
                
                if (dy1 == minDiff) {
                    // Top collision has occurred
                    pos.y -= dy1;
                    mYSpeed = 0.0f;
                }
                mOwner->SetPosition(pos);
            }
        }
        
        Player* p = mOwner->GetGame()->GetPlayer();
        if (mOwner->GetCollision()->Intersect(p->GetCollision())) {
            p->SetPosition(p->GetInitialPos());
        }
    }
    if (mOwner->GetPosition().y - 32 > 768) {
        mOwner->SetState(Actor::EDead);
    }
}
