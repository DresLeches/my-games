#pragma once
#include "SDL/SDL.h"
#include "SDL/SDL_mixer.h"
#include <unordered_map>
#include <string>
#include <vector>

class Game
{
public:
	Game();
	bool Initialize();
	void RunLoop();
	void Shutdown();

	void AddActor(class Actor* actor);
	void RemoveActor(class Actor* actor);

	void AddSprite(class SpriteComponent* sprite);
	void RemoveSprite(class SpriteComponent* sprite);

	void LoadTexture(const char* fileName);
	SDL_Texture* GetTexture(const char* fileName);
	void LoadLevel(const char* fileName);
    
    // Accessor function
    class Player* GetPlayer();
	
	// Vector of blocks
	std::vector<class Block*> mBlocks;
    
    // Do some sounds
    void LoadSound(const std::string& filename);
    Mix_Chunk* GetSound(const std::string& filename);
    void PlayJumpSound();
    
private:
	void ProcessInput();
	void UpdateGame();
	void GenerateOutput();
	void LoadData();
	void UnloadData();
    
    // Actors
    class Player* p;
    class BarrelSpawner* bs;

	// Map of textures loaded
	std::unordered_map<std::string, SDL_Texture*> mTextures;
    
    // Map of the sound
    std::unordered_map<std::string, Mix_Chunk*> mSound;

	// All the actors in the game
	std::vector<class Actor*> mActors;

	// All the sprite components drawn
	std::vector<class SpriteComponent*> mSprites;
	
	SDL_Window* mWindow;
	SDL_Renderer* mRenderer;
	Uint32 mTicksCount;
	bool mIsRunning;
};
