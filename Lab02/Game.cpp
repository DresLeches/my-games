//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
#include <stdio.h>
#include <iostream>

// TODO
Game::Game() { }

// Initialize the game
bool Game::initialize() {
    
    // It didn't work :(
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return false;
    }
    
    // Initialize the game's member variables
    playerQuit = false;
    playerLoses = false;
    topWall = new SDL_Rect();
    rightWall = new SDL_Rect();
    bottomWall = new SDL_Rect();
    paddle1 = new SDL_Rect();
    ball = new SDL_Rect();
    
    // Initialize the windows dimensions
    _w = 1024;
    _h = 768;
    
    // Initialize the paddle's positions and properties
    paddleW = 10;
    paddleH = 100;
    paddle1Pos.x = 30;
    paddle1Pos.y = _w/2 - 50;
    paddleDirUp = false;
    paddleDirDown = false;
    paddleSpeed = 250;
    ballSpeed = 200;
     
     
    
    // Initialize the ball's position and properties
    ballW = 10;
    ballH = 10;
    ballPos.x = _w/2 - 10;
    ballPos.y = _w/2 - 10;
    ballDirUp = true;
    ballDirDown = false;
    ballDirForward = true;
    ballDirBackward = false;
    
    // Creating the SDL Window
    sdlw = SDL_CreateWindow("Pong Game",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             _w,
                             _h,
                             0);
    
    // Create the SDL Renderer
    sdlr = SDL_CreateRenderer(sdlw, 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    
    // Get the number of ticks
    prevFrameTime = SDL_GetTicks() / 1000.0;
    
    // We created it :)
    return true;
}
void Game::shutDown() {
    SDL_DestroyRenderer(sdlr);
    SDL_DestroyWindow(sdlw);
    SDL_Quit();

    delete topWall;
    delete rightWall;
    delete bottomWall;
    delete paddle1;
    delete ball;
}
void Game::runLoop() {
    while (!playerQuit && !playerLoses) {
        processInput();
        updateGame();
        generateOutput();
    }
}
void Game::processInput() {
    SDL_Event sdlEvent;
    bool quit = false;
    
    while (SDL_PollEvent(&sdlEvent)) {
        switch(sdlEvent.type) {
            case SDL_QUIT:
                //printf("Quit\n");
                quit = true;
                break;
        }
    }
    const Uint8 *state = SDL_GetKeyboardState(NULL);
    if (state[SDL_SCANCODE_ESCAPE] || quit) {
        playerQuit = true;
    }
    else if (state[SDL_SCANCODE_UP]) {
        paddleDirUp = true;
        paddleDirDown = false;
    }
    else if (state[SDL_SCANCODE_DOWN]) {
        paddleDirDown = true;
        paddleDirUp = false;
    }
}
/*
 Bug: The ball goes through the paddle if the corner of the ball does touch the paddle
 */
void Game::updateGame() {
    while (SDL_GetTicks() < 16) { }
    
    float currFrameTime = SDL_GetTicks() / 1000.0;
    delta = currFrameTime - prevFrameTime;
    prevFrameTime = currFrameTime;
    
    // Handle the paddle movement
    if (paddleDirUp == true && paddleDirDown == false) {
        if (topWall->y + 10 < paddle1Pos.y) {
            paddle1Pos.y = paddle1Pos.y - (int)(ceil(paddleSpeed * delta));
        }
    }
    else if (paddleDirUp == false && paddleDirDown == true) {
        if (bottomWall->y > paddle1Pos.y + 100) {
            paddle1Pos.y = paddle1Pos.y + (int)(ceil(paddleSpeed * delta));
        }
    }
    paddleDirUp = false;
    paddleDirDown = false;
    
    // Handle the ball movement
    
    // Check if player loses
    if (ballPos.x <= 0) {
        playerLoses = true;
        return;
    }
    
    // Change the direction of the y component
    if (topWall->y + 10 > ballPos.y) {
        ballDirUp = false;
        ballDirDown = true;
    }
    else if (bottomWall->y < ballPos.y + 10) {
        ballDirUp = true;
        ballDirDown = false;
    }
    
    // Change the direction of the x component
    if (rightWall->x < ballPos.x + 10) {
        ballDirForward = false;
        ballDirBackward = true;
    }
    else if (ballHitPaddle()) {
        ballDirForward = true;
        ballDirBackward = false;
    }
    
    if (ballDirUp == true) {
        if (ballDirForward == true) {
            ballPos.x = ballPos.x + (int)(ceil(ballSpeed * delta));
            ballPos.y = ballPos.y - (int)(ceil(ballSpeed * delta));
        }
        else if (ballDirBackward == true) {
            ballPos.x = ballPos.x - (int)(ceil(ballSpeed * delta));
            ballPos.y = ballPos.y - (int)(ceil(ballSpeed * delta));
        }
    }
    else if (ballDirDown == true) {
        if (ballDirForward == true) {
            ballPos.x = ballPos.x + (int)(ceil(ballSpeed * delta));
            ballPos.y = ballPos.y + (int)(ceil(ballSpeed * delta));
        }
        else if (ballDirBackward == true) {
            ballPos.x = ballPos.x - (int)(ceil(ballSpeed * delta));
            ballPos.y = ballPos.y + (int)(ceil(ballSpeed * delta));
        }
    }
    
    //SDL_Log("Current time: %f", delta);
}
void Game::generateOutput() {
    if (SDL_SetRenderDrawColor(sdlr, 0, 0, 255, 255) != 0) {
        SDL_Log("Unable to generate output SDL: %s", SDL_GetError());
        return;
    }
    if (SDL_RenderClear(sdlr)) {
        SDL_Log("Unable to clear renderer SDL: %s", SDL_GetError());
        return;
    }
    
    // Draw the objects
    drawWall();
    drawPaddle(paddle1);
    drawBall();
    
    // Render the image
    SDL_RenderPresent(sdlr);
}

void Game::drawWall() {
    const int thickness = 10;

    // Initialize all the walls
    updateObj(topWall, 0, 0, _w, thickness);
    updateObj(rightWall, _w - thickness, 0, thickness, _h);
    updateObj(bottomWall, 0, _h - thickness, _w, thickness);
    
    // Set the color
    SDL_SetRenderDrawColor(sdlr, 255, 255, 255, 255);
    
    if (SDL_RenderFillRect(sdlr, topWall) != 0 ||
        SDL_RenderFillRect(sdlr, rightWall) != 0 ||
        SDL_RenderFillRect(sdlr, bottomWall) != 0)
    {
        SDL_Log("Unable to draw the window SDL: %s", SDL_GetError());
        return;
    }
}

void Game::drawPaddle(SDL_Rect* paddle) {
    
    // Initialize all the walls
    updateObj(paddle, paddle1Pos.x, paddle1Pos.y, paddleW, paddleH);
    
    // Set the color
    SDL_SetRenderDrawColor(sdlr, 255, 255, 255, 255);
    
    if (SDL_RenderFillRect(sdlr, paddle1) != 0) {
        SDL_Log("Unable to draw the window SDL: %s", SDL_GetError());
        return;
    }
}

void Game::drawBall() {
    // Initialize all the walls
    updateObj(ball, ballPos.x, ballPos.y, ballW, ballH);
    
    // Set the color
    SDL_SetRenderDrawColor(sdlr, 255, 255, 255, 255);
    
    if (SDL_RenderFillRect(sdlr, ball) != 0) {
        SDL_Log("Unable to draw the window SDL: %s", SDL_GetError());
        return;
    }
}
void Game::updateObj(SDL_Rect *obj, const int x, const int y, const int w, const int h) {
    obj->x = x;
    obj->y = y;
    obj->w = w;
    obj->h = h;
}


bool Game::ballHitPaddle() {
    return (paddle1Pos.x + 10 > ballPos.x || paddle1Pos.x > ballPos.x) &&
            ((paddle1Pos.y + 100 > ballPos.y && paddle1Pos.y < ballPos.y) ||
             (paddle1Pos.y + 100 > ballPos.y + 10 && paddle1Pos.y < ballPos.y + 10));
}
