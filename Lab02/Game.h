#pragma once

// Header files
#include "SDL/SDL.h"
#include <cmath>

class Game {
public:
    Game();
    bool initialize();
    void shutDown();
    void runLoop();
    
    // ProcessInput, UpdateGame, GenerateOuput)
    void processInput();
    void updateGame();
    void generateOutput();
    void drawWall();
    void drawPaddle(SDL_Rect* paddle);
    void drawBall();
    void updateObj(SDL_Rect* obj, const int x, const int y, const int w, const int h);
    bool ballHitPaddle();
    
private:
    // Window dim
    int _w;
    int _h;
    
    // Window object
    SDL_Window* sdlw;
    SDL_Renderer* sdlr;
    
    // Wall
    SDL_Rect* topWall;
    SDL_Rect* rightWall;
    SDL_Rect* bottomWall;
    
    // Paddle1
    SDL_Rect* paddle1;
    SDL_Point paddle1Pos;
    
    // Paddle Properties
    int paddleW;
    int paddleH;
    bool paddleDirUp;
    bool paddleDirDown;
    
    // Ball
    SDL_Rect* ball;
    SDL_Point ballPos;
    
    // Ball Properties
    int ballW;
    int ballH;
    bool ballDirUp;
    bool ballDirDown;
    bool ballDirForward;
    bool ballDirBackward;
        
    // Player quit
    bool playerQuit;
    bool playerLoses;
    
    // Calculating the frame rate
    float delta;
    float prevFrameTime;
    int paddleSpeed;
    int ballSpeed;
};
