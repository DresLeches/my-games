//
//  CameraComponent.cpp
//  Game-mac
//
//  Created by Pablo Chung on 3/29/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "CameraComponent.hpp"
#include "Actor.h"
#include "MoveComponent.h"
#include "Math.h"
#include "Game.h"
#include "Renderer.h"

CameraComponent::CameraComponent(class Actor* owner)
:Component(owner)
,mPitchSpeed(0.0f)
,mPitchAngle(0.0f)
{

}
CameraComponent::~CameraComponent() {
   
}

void CameraComponent::Update(float deltaTime) {
    
    // Change the pitch angle
    mPitchAngle += mPitchSpeed * deltaTime;
    if (mPitchAngle < -Math::PiOver2/2) {
        mPitchAngle = -Math::PiOver2/2;
    }
    else if (mPitchAngle > Math::PiOver2/2) {
        mPitchAngle = Math::PiOver2/2;
    }
    
    // Create the matrix for rotation for pitch and yaw
    Matrix4 pitchMatrix = Matrix4::CreateRotationY(mPitchAngle);
    Matrix4 yawMatrix = Matrix4::CreateRotationZ(mOwner->GetRotation());
    Matrix4 rotationMatrix = pitchMatrix * yawMatrix;
    Vector3 tempVec(1.0f, 0.0f, 0.0f);
    Vector3 transform = Vector3::Transform(tempVec, rotationMatrix);
    
    // Create the matrix for camera
    Vector3 eye = mOwner->GetPosition();
    Vector3 target = transform + mOwner->GetPosition();
    Vector3 up(0.0f, 0.0f, 1.0f);
    Matrix4 lookAt = Matrix4::CreateLookAt(eye, target, up);
    mOwner->GetGame()->GetRenderer()->SetViewMatrix(lookAt);
    

}

void CameraComponent::ProcessInput(const Uint8* keyState) {
    
}
