//
//  CameraComponent.hpp
//  Game-mac
//
//  Created by Pablo Chung on 3/29/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef CameraComponent_hpp
#define CameraComponent_hpp

#include <stdio.h>
#include "Component.h"
class CameraComponent : public Component {
public:
    CameraComponent(class Actor* owner);
    ~CameraComponent();
    void Update(float deltaTime);
    void ProcessInput(const Uint8* keyState);
    float GetPitchSpeed() { return mPitchSpeed; }
    void SetPitchSpeed(float pitchSpeed) { mPitchSpeed = pitchSpeed; }
private:
    float mPitchAngle;
    float mPitchSpeed;
};

#endif /* CameraComponent_hpp */
