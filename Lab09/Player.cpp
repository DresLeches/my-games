//
//  Player.cpp
//  Game-mac
//
//  Created by Pablo Chung on 3/29/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Player.hpp"
#include "MoveComponent.h"
#include "CollisionComponent.h"
#include "CameraComponent.hpp"
#include "PlayerMove.hpp"

Player::Player(class Game* game)
:Actor(game)
{
    mMove = new PlayerMove(this);
    mCollision = new CollisionComponent(this);
    mCollision->SetSize(50.0f, 175.0f, 50.0f);
    mCamera = new CameraComponent(this);
}
Player::~Player() {
    
}
