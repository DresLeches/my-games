//
//  PlayerMove.hpp
//  Game-mac
//
//  Created by Pablo Chung on 3/29/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef PlayerMove_hpp
#define PlayerMove_hpp

#include <stdio.h>
#include "MoveComponent.h"

class PlayerMove : public MoveComponent {
public:
    enum MoveState
    {
        OnGround,
        Jump,
        Falling
    };
    enum CollSide {
        None,
        Top,
        Bottom,
        Side
    };
    PlayerMove(class Actor* owner);
    ~PlayerMove();
    void Update(float deltaTime);
    void ProcessInput(const Uint8* keyState);
    void ChangeState(MoveState currentState);
protected:
    MoveState mCurrentState;
    float mZSpeed;
    const float Gravity;
    const float JumpSpeed;
    bool mSpacePressed;
    bool mInAir;
    
    void UpdateOnGround(float deltaTime);
    void UpdateJump(float deltaTime);
    void UpdateFalling(float deltaTime);
    CollSide FixCollision(class CollisionComponent* self, class CollisionComponent* block);
};
#endif /* PlayerMove_hpp */
