//
//  PlayerMove.cpp
//  Game-mac
//
//  Created by Pablo Chung on 3/29/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "PlayerMove.hpp"
#include "Game.h"
#include "Actor.h"
#include "CameraComponent.hpp"
#include "CollisionComponent.h"
#include "Block.h"
#include <iostream>

PlayerMove::PlayerMove(class Actor* owner)
:MoveComponent(owner)
,mZSpeed(0.0f)
,Gravity(-980.0f)
,JumpSpeed(500.0f)
,mSpacePressed(false)
,mInAir(false)

{
    ChangeState(Falling);
}
PlayerMove::~PlayerMove() {
    
}
void PlayerMove::Update(float deltaTime) {
    if (mCurrentState == MoveState::Falling) {
        UpdateFalling(deltaTime);
    }
    else if (mCurrentState == MoveState::OnGround) {
        UpdateOnGround(deltaTime);
    }
    else if (mCurrentState == MoveState::Jump) {
        UpdateJump(deltaTime);
    }
}
void PlayerMove::ProcessInput(const Uint8* keyState) {
    if (keyState[SDL_SCANCODE_W] && keyState[SDL_SCANCODE_S]) {
        SetForwardSpeed(0.0f);
    }
    else if (keyState[SDL_SCANCODE_W]) {
        SetForwardSpeed(350.0f);
    }
    else if (keyState[SDL_SCANCODE_S]) {
        SetForwardSpeed(-350.0f);
    }
    else {
        SetForwardSpeed(0.0f);
    }
    
    if (keyState[SDL_SCANCODE_A] && keyState[SDL_SCANCODE_D]) {
        SetStrafeSpeed(0.0f);
    }
    else if (keyState[SDL_SCANCODE_A]) {
        SetStrafeSpeed(-350.0f);
    }
    else if (keyState[SDL_SCANCODE_D]) {
        SetStrafeSpeed(350.0f);
    }
    else {
        SetStrafeSpeed(0.0f);
    }
    
    int x, y;
    SDL_GetRelativeMouseState(&x, &y);
    
    // Set the angular speed
    float angSpeed = static_cast<float>(x);
    angSpeed /= 500.0f;
    angSpeed *= Math::Pi * 10.0f;
    SetAngularSpeed(angSpeed);
    
    // Set the pitch speed
    float pitchSpeed = static_cast<float>(y);
    pitchSpeed /= 500.0f;
    pitchSpeed *= Math::Pi * 10.0f;
    mOwner->GetCamera()->SetPitchSpeed(pitchSpeed);

    // Jumping motion
    if (!mSpacePressed && !mInAir && keyState[SDL_SCANCODE_SPACE]) {
        mZSpeed = JumpSpeed;
        mInAir = true;
        ChangeState(PlayerMove::Jump);
    }
    mSpacePressed = keyState[SDL_SCANCODE_SPACE];
    
}

void PlayerMove::ChangeState(MoveState currentState) {
    mCurrentState = currentState;
}

void PlayerMove::UpdateOnGround(float deltaTime) {
    MoveComponent::Update(deltaTime);
    bool stoodOnTop = false;
    std::vector<Block*> blocks = mOwner->GetGame()->mBlocks;
    for (auto block : blocks) {
        PlayerMove::CollSide tempState = FixCollision(mOwner->GetCollision(), block->GetCollision());
        if (tempState == PlayerMove::Top) {
            stoodOnTop = true;
            mInAir = false;
        }
    }
    if (!stoodOnTop) {
        ChangeState(PlayerMove::Falling);
    }
}
void PlayerMove::UpdateJump(float deltaTime) {
    MoveComponent::Update(deltaTime);
    mZSpeed += Gravity * deltaTime;
    Vector3 pos = mOwner->GetPosition();
    pos.z += mZSpeed * deltaTime;
    mOwner->SetPosition(pos);
    std::vector<Block*> blocks = mOwner->GetGame()->mBlocks;
    for (auto block : blocks) {
        PlayerMove::CollSide tempState = FixCollision(mOwner->GetCollision(), block->GetCollision());
        if (tempState == PlayerMove::Bottom) {
            mZSpeed = 0.0f;
        }
    }
    if (mZSpeed <= 0.0f) {
        ChangeState(PlayerMove::Falling);
    }
}
void PlayerMove::UpdateFalling(float deltaTime) {
    MoveComponent::Update(deltaTime);
    mZSpeed += Gravity * deltaTime;
    Vector3 pos = mOwner->GetPosition();
    pos.z += mZSpeed * deltaTime;
    mOwner->SetPosition(pos);
    std::vector<Block*> blocks = mOwner->GetGame()->mBlocks;
    for (auto block : blocks) {
        PlayerMove::CollSide tempState = FixCollision(mOwner->GetCollision(), block->GetCollision());
        if (tempState == PlayerMove::Top) {
            mZSpeed = 0.0f;
            ChangeState(PlayerMove::OnGround);
            mInAir = false;
        }
    }
}

PlayerMove::CollSide PlayerMove::FixCollision(class CollisionComponent* self, class CollisionComponent* block) {
    PlayerMove::CollSide tempState = PlayerMove::None;
    Vector3 pos = mOwner->GetPosition();

    if (self->Intersect(block)) {
        Vector3 aMin = self->GetMin();
        Vector3 aMax = self->GetMax();
        Vector3 bMin = block->GetMin();
        Vector3 bMax = block->GetMax();
        
        float dx1 = Math::Abs(aMax.x - bMin.x);
        float dx2 = Math::Abs(aMin.x - bMax.x);
        float dy1 = Math::Abs(aMax.y - bMin.y);
        float dy2 = Math::Abs(aMin.y - bMax.y);
        float dz1 = Math::Abs(aMax.z - bMin.z);
        float dz2 = Math::Abs(aMin.z - bMax.z);
        float minDiff = std::min({dx1, dx2, dy1, dy2, dz1, dz2});
        
        if (dx1 == minDiff) {
            // Left collision has occurred
            pos.x -= dx1;
            tempState = PlayerMove::Side;
        }
        else if (dx2 == minDiff) {
            // Right collision has occurred
            pos.x += dx2;
            tempState = PlayerMove::Side;
        }
        else if (dy1 == minDiff) {
            // Top collision has occurred
            pos.y -= dy1;
            tempState = PlayerMove::Side;
        }
        else if (dy2 == minDiff) {
            // Bottom collision has occurred
            pos.y += dy2;
            tempState = PlayerMove::Side;
        }
        else if (dz1 == minDiff) {
            // Top z collision has occurred
            pos.z -= dz1;
            tempState = PlayerMove::Bottom;
        }
        else if (dz2 == minDiff) {
            // Bottom z collision has occurred
            pos.z += dz2;
            tempState = PlayerMove::Top;
        }
        mOwner->SetPosition(pos);
    }
    return tempState;
}
