//
//  Player.hpp
//  Game-mac
//
//  Created by Pablo Chung on 3/29/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Player_hpp
#define Player_hpp

#include <stdio.h>
#include "Actor.h"
class Player : public Actor {
public:
    Player(class Game* game);
    ~Player();
};


#endif /* Player_hpp */
